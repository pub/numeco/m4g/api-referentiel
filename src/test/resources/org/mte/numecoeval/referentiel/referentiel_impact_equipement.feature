#language: fr
#encoding: utf-8
Fonctionnalité: Gestion du referentiel ref_ImpactEquipement
  Chargement et Mise à disposition du référentiel des impacts d'équipements.


  Scénario: Sprint 10 - Taiga 869: Ajout d'une colonne description
    Quand le fichier CSV des impacts d'équipements "csv/sprint10/ref_ImpactEquipement.csv" est importé par API et la réponse est 200
    Alors La récupération de tous les impacts d'équipements renvoie 6 lignes
    Et Vérifications des impacts équipements disponibles
      | etape       | critere               | refEquipement                     | valeur               | consoElecMoyenne  | source             | type                           | description                                                  |
      | FIN_DE_VIE  | Acidification         | computer monitor-01-23            | 0.020296259210277726 | 35.81692801813716 | negaoctet_20221013 | computer monitor               | Description Impact Equipement computer monitor               |
      | FABRICATION | Radiations ionisantes | digital price tag - power unit-01 | 5.89E+01             |                   | negaoctet_20221013 | digital price tag - power unit | Description Impact Equipement digital price tag - power unit |
      | FABRICATION | Radiations ionisantes | docking station-01                | 3.50E+01             | 1.28              | negaoctet_20221013 | docking station                | Description Impact Equipement docking station                |
      | FABRICATION | Radiations ionisantes | empty 42u bay-01                  | 5.87E+01             |                   | negaoctet_20221013 | empty 42u bay                  | Description Impact Equipement empty 42u bay                  |
      | FABRICATION | Radiations ionisantes | feature phone-01                  | 5.11E+00             | 0.09              | negaoctet_20221013 | feature phone                  | Description Impact Equipement feature phone                  |
      | FABRICATION | Radiations ionisantes | firewall-01                       | 2.65E+02             | 876               | negaoctet_20221013 | firewall                       | Description Impact Equipement firewall                       |








