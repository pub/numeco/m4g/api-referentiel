#language: fr
#encoding: utf-8
Fonctionnalité: Sprint 10 - Features du Sprint 10 (type équipement par défaut, correspondances...)
  Nouvelles features disponibles:
  - Reférentiel TypeEquipement avec le refEquipementParDefaut correctement géré
  - Nouveau référentiel CorrespondanceRefEquipement


  Scénario: Sprint 10 - Taiga 862: Ajout d'une colonne refEquipementParDefaut dans ref_TypeEquipement
    Quand le fichier CSV des types d'équipements "csv/sprint10/ref_TypeEquipement.csv" est importé par API et la réponse est 200
    Alors La récupération de tous les types d'équipements renvoie 17 lignes
    Et La récupération unitaire d'un type équipement "ServeurCalcul" répond 200
    Et Vérifications des types d'équipements disponibles
      | type                     | serveur | commentaire                   | dureeVieDefaut | refEquipementParDefaut    | source |
      | ServeurCalcul            | true    | commentaires                  | 6              | serveur_par_defaut        | Test   |
      | ServeurStockage          | true    |                               | 6              | serveur_par_defaut        | Test   |
      | Armoire                  | false   |                               | 7              | armoir_par_defaut         | Test   |
      | NAS                      | false   |                               | 6              | baie_stockage_par_defaut  | Test   |
      | Switch                   | false   |                               | 6              | switch_par_defaut         | Test   |
      | OrdinateurPortable       | false   |                               | 4              | laptop_par_defaut         | Test   |
      | OrdinateurDeBureau       | false   |                               | 5              | desktop_par_defaut        | Test   |
      | Tablette                 | false   |                               | 2.5            | tablette_par_defaut       | Test   |
      | TelephoneMobile          | false   | commentaires                  | 2.5            | smartphone_par_defaut     | Test   |
      | TelephoneFixe            | false   |                               | 10             | telephone_fixe_par_defaut | Test   |
      | EquipementReseau         | false   | commentaires                  | 5              | switch_par_defaut         | Test   |
      | BorneWifi                | false   |                               | 4              | borneWifi_par_defaut      | Test   |
      | Imprimante               | false   |                               | 5              | imprimante_par_defaut     | Test   |
      | Ecran                    | false   |                               | 8              | ecran_par_defaut          | Test   |
      | AccessoirePosteDeTravail | false   | souris calvier, camera casque | 7              | stationTravail_par_defaut | Test   |
      | Connectique              | false   |                               | 7              |                           | Test   |
      | Autre                    | false   |                               | 2              |                           | Test   |


  Scénario: Sprint 10 - Taiga 319: Import des données de correspondances de références d'équipements
    Quand le fichier CSV des correspondances d'équipements "csv/sprint10/ref_CorrespondanceRefEquipement.csv" est importé par API et la réponse est 200
    Alors La récupération de tous les correspondances d'équipements renvoie 3 lignes
    Et Vérifications des correspondances d'équipements disponibles
      | modeleEquipementSource | refEquipementCible |
      | Fairphone V1           | smartphone01       |
      | Fairphone V2           | smartphone01       |
      | Ecran 27 pouces        | ecran01            |

