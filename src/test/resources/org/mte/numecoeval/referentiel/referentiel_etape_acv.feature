#language: fr
#encoding: utf-8
Fonctionnalité: Gestion du referentiel ETAPE ACV
  Le but étant de rendre disponible le référentiel referentiel ETAPE ACV afin de rendre possible
  les calculs d'impacts environnementaux sur les équipements


  Scénario: CAF1: alimentation en masse du referentiel etape acv depuis un ficher csv
    Quand le fichier CSV "csv/ref_etapeACV.csv" est injecte
    Alors Il existe 4 elements dans le referentiel Etape ACV
    Et  chacun des elements du referentiel ajouté est identique au element du fichier csv





