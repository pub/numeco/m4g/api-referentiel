#language: fr
#encoding: utf-8
Fonctionnalité: Gestion du referentiel DES HYPOTHESES
  Le but étant de rendre disponible le référentiel referentiel DES HYPOTHESES afin de rendre possible
  les calculs d'impacts environnementaux sur les équipements


  Scénario: CAF1: alimentation en masse du referentiel DES HYPOTHESES depuis un ficher csv
    Quand le fichier CSV REF HYPOTHESE"csv/ref_Hypothese.csv" est inject
    Alors le statut http est 200


  Scénario: CAF2: recherche d'une hypothese
    Quand lors de la recherche de hypothese avec la cle "PUEPardDfault"
    Alors Le resultat est : cle = "PUEPardDfault" valeur="1.6" et source="expertise IJO"




