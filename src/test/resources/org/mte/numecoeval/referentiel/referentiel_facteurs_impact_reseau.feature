#language: fr
#encoding: utf-8
Fonctionnalité: Gestion du referentiel impact reseau
  Le but étant de rendre disponible le référentiel refImpactReseau afin de rendre possible
  les calculs d'impacts environnementaux sur les équipements


  Scénario: CAF1: Ajouter un facteur impact lié au reseau dans la table ref_impactReseau
    Quand on ajoute un facteur impact reseau dont la refReseau est "impactReseauMobileMoyen", etapeACV est  "UTILISATION" ,  critere est "Acidification" la source est "NegaOctet V1.00" la valeur est "8.28" et l'unité est "mol H^{+} eq"
    Alors impact reseau est persister dans la table ref_impactReseau  avec les elements suivants: la refReseau est "impactReseauMobileMoyen", etapeACV est  "UTILISATION" ,  critere est "Acidification" la source est "NegaOctet V1.00" la valeur est "8.28" et l'unité est "mol H^{+} eq"


  Scénario: CAF2: Ajouter un facteur impact lié au reseau dans la table ref_impactReseau sans unité ni valeur et modification des valeurs unite et valeur de l'impact reseau ajouté
    Quand on persiste un facteur impact reseau dont la refReseau est "impactReseauMobileFRANCE", etapeACV est  "FABRICATION" ,  critere est "Radiations ionisantes" la source est "NegaOctet V1.00"
    Alors il est possible de modifier impact  Reseau avec les élements suivants: le refReseau est "impactReseauMobileFRANCE", etapeACV est  "FABRICATION" ,  critere est "Radiations ionisantes" la source est "NegaOctet V1.00", valeur est "10" et l'unité est "kBq U-235 eq"

  Scénario: CAF3: interdir l'ajout d'un facteur impact lié au reseau dans la table ref_impactReseau sans source
    Quand on ajoute un facteur impact reseau dont la refReseau est "impactReseauMobileMoyenSansSource", etapeACV est  "UTILISATION" ,  critere est "Changement Climatique"  la valeur est "8.28" et l'unité est "CO2 eq / an"
    Alors Impossible d ajouter l'impact réseau dans la table ref_impactreseau

  Scénario: CAF4: alimentation en masse du referentiel impact reseau depuis un ficher csv
    Quand le fichier CSV "csv/ref_impactreseau.csv" est envoyé
    Alors l impact reseau  refReseal est "impactReseauMobile2G", etapeACV est  "UTILISATION" ,  critere est "Radiations ionisantes" la source est "NegaOctet V1.00" la valeur est "18.8" et l'unité est "kBq U-235 eq" existe


  Scénario: CAF5: impact reseau qui n'existe pas
    Quand on cherche l impact reseau  dont la refReseau est "nexistepas", etapeACV est  "nexistepas" ,  critere est "nexistepas"
    Alors lecture reponse impact resau n existe pas



