package org.mte.numecoeval.referentiel.factory;

import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.CritereDTO;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.model.CorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.domain.model.Critere;
import org.mte.numecoeval.referentiel.domain.model.Etape;
import org.mte.numecoeval.referentiel.domain.model.Hypothese;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.ImpactMessagerie;
import org.mte.numecoeval.referentiel.domain.model.ImpactReseau;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.CritereId;
import org.mte.numecoeval.referentiel.domain.model.id.EtapeId;
import org.mte.numecoeval.referentiel.domain.model.id.HypotheseId;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactReseauId;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.HypotheseEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CritereIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.EtapeIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.HypotheseIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.CritereIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.EtapeIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.HypotheseIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactReseauIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.MixElectriqueIdDTO;

public class TestDataFactory {

    public static final String DEFAULT_ETAPE = "UTILISATION";

    public static final String DEFAULT_CRITERE = "Changement Climatique";
    public static final String DEFAULT_UNITE = "kg CO² eq";

    public static class CritereFactory {
        public static CritereDTO dto(String nomCritere, String unite, String description) {
            return new CritereDTO(nomCritere, unite, description);
        }

        public static Critere domain(String nomCritere, String description, String unite) {
            return new Critere()
                    .setNomCritere(nomCritere)
                    .setDescription(description)
                    .setUnite(unite);
        }

        public static CritereEntity entity(String nomCritere, String description, String unite) {
            return new CritereEntity()
                    .setNomCritere(nomCritere)
                    .setDescription(description)
                    .setUnite(unite);
        }

        public static CritereIdDTO idDTO(String nomCritere) {
            return new CritereIdDTO(nomCritere);
        }

        public static CritereId idDomain(String nomCritere) {
            return new CritereId()
                    .setNomCritere(nomCritere);
        }

        public static CritereIdEntity idEntity(String nomCritere) {
            return new CritereIdEntity()
                    .setNomCritere(nomCritere);
        }
    }

    public static class EtapeFactory {

        public static EtapeDTO dto(String code, String libelle) {
            return new EtapeDTO(code, libelle);
        }

        public static Etape domain(String code, String libelle) {
            return new Etape()
                    .setCode(code)
                    .setLibelle(libelle);
        }

        public static EtapeEntity entity(String code, String libelle) {
            return new EtapeEntity()
                    .setCode(code)
                    .setLibelle(libelle);
        }

        public static EtapeIdDTO idDTO(String code) {
            return new EtapeIdDTO(code);
        }

        public static EtapeId idDomain(String code) {
            return new EtapeId()
                    .setCode(code);
        }

        public static EtapeIdEntity idEntity(String code) {
            return new EtapeIdEntity()
                    .setCode(code);
        }
    }

    public static class HypotheseFactory {
        public static HypotheseDTO dto(String code, String valeur, String source) {
            return new HypotheseDTO(code, valeur, source);
        }

        public static Hypothese domain(String code, String valeur, String source) {
            return new Hypothese()
                    .setCode(code)
                    .setValeur(valeur)
                    .setSource(source);
        }

        public static HypotheseEntity entity(String code, String valeur, String source) {
            return new HypotheseEntity()
                    .setCode(code)
                    .setValeur(valeur)
                    .setSource(source);
        }

        public static HypotheseIdDTO idDTO(String code) {
            return new HypotheseIdDTO()
                    .setCode(code);
        }

        public static HypotheseId idDomain(String code) {
            return new HypotheseId()
                    .setCode(code);
        }

        public static HypotheseIdEntity idEntity(String code) {
            return new HypotheseIdEntity()
                    .setCode(code);
        }
    }

    public static class TypeEquipementFactory {
        public static TypeEquipementDTO dto(String type, boolean estUnServeur, Double dureeVieDefaut, String commentaire, String source, String refEquipementParDefaut) {
            return TypeEquipementDTO.builder()
                    .type(type)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .source(source)
                    .commentaire(commentaire)
                    .refEquipementParDefaut(refEquipementParDefaut)
                    .build();
        }

        public static TypeEquipement domain(String type, boolean estUnServeur, Double dureeVieDefaut, String commentaire, String source, String refEquipementParDefaut) {
            return TypeEquipement.builder()
                    .type(type)
                    .serveur(estUnServeur)
                    .dureeVieDefaut(dureeVieDefaut)
                    .source(source)
                    .commentaire(commentaire)
                    .refEquipementParDefaut(refEquipementParDefaut)
                    .build();
        }

        public static TypeEquipementEntity entity(String type, boolean estUnServeur, Double dureeVieDefaut, String commentaire, String source, String refEquipementParDefaut) {
            return new TypeEquipementEntity(type,estUnServeur,commentaire,dureeVieDefaut,source, refEquipementParDefaut);
        }
    }

    public static class MixElectriqueFactory {

        public static MixElectriqueDTO dto(CritereDTO critere, String pays, String raccourcisAnglais, Double valeur, String source) {
            return new MixElectriqueDTO(
                    pays,
                    raccourcisAnglais,
                    critere.getNomCritere(),
                    valeur,
                    source
            );
        }

        public static MixElectrique domain(Critere critere, String pays, String raccourcisAnglais, Double valeur, String source) {
            return new MixElectrique()
                    .setCritere(critere.getNomCritere())
                    .setPays(pays)
                    .setRaccourcisAnglais(raccourcisAnglais)
                    .setValeur(valeur)
                    .setSource(source);
        }

        public static MixElectriqueEntity entity(CritereEntity critere, String pays, String raccourcisAnglais, Double valeur, String source) {
            return new MixElectriqueEntity()
                    .setCritere(critere.getNomCritere())
                    .setPays(pays)
                    .setRaccourcisAnglais(raccourcisAnglais)
                    .setValeur(valeur)
                    .setSource(source);
        }

        public static MixElectriqueIdDTO idDTO(CritereIdDTO critereId, String pays) {
            return MixElectriqueIdDTO
                    .builder()
                    .critere(critereId.getNomCritere())
                    .pays(pays)
                    .build();
        }

        public static MixElectriqueId idDomain(CritereId critereId, String pays) {
            return new MixElectriqueId()
                    .setCritere(critereId.getNomCritere())
                    .setPays(pays);
        }

        public static MixElectriqueIdEntity idEntity(CritereIdEntity critereId, String pays) {
            return new MixElectriqueIdEntity()
                    .setCritere(critereId.getNomCritere())
                    .setPays(pays);
        }
    }

    public static class ImpactEquipementFactory {

        public static ImpactEquipementDTO dto(String etape, String critere, String refEquipement, String source, String type, Double valeur, Double consoElecMoyenne, String description) {
            return new ImpactEquipementDTO(
                    refEquipement,
                    etape,
                    critere,
                    source,
                    type,
                    valeur,
                    consoElecMoyenne,
                    description
            );
        }

        public static ImpactEquipement domain(Etape etape, Critere critere, String refEquipement, String source, String type, Double valeur, Double consoElecMoyenne) {
            return new ImpactEquipement()
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setRefEquipement(refEquipement)
                    .setSource(source)
                    .setType(type)
                    .setValeur(valeur)
                    .setConsoElecMoyenne(consoElecMoyenne);
        }

        public static ImpactEquipementEntity entity(EtapeEntity etape, CritereEntity critere, String refEquipement, String source, String type, Double valeur, Double consoElecMoyenne) {
            return new ImpactEquipementEntity()
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setRefEquipement(refEquipement)
                    .setSource(source)
                    .setType(type)
                    .setValeur(valeur)
                    .setConsoElecMoyenne(consoElecMoyenne);
        }

        public static ImpactEquipementIdDTO idDTO(String etapeIdDTO, String critereId, String refEquipement) {
            return ImpactEquipementIdDTO
                    .builder()
                    .etape(etapeIdDTO)
                    .critere(critereId)
                    .refEquipement(refEquipement)
                    .build();
        }

        public static ImpactEquipementId idDomain(String etapeId, String critereId, String refEquipement) {
            return new ImpactEquipementId()
                    .setEtape(etapeId)
                    .setCritere(critereId)
                    .setRefEquipement(refEquipement);
        }

    }

    public static class ImpactReseauFactory {

        public static ImpactReseauDTO dto(EtapeDTO etape, CritereDTO critere, String refReseau, String source, Double valeur, Double consoElecMoyenne) {
            return new ImpactReseauDTO(
                    refReseau,
                    etape.getCode(),
                    critere.getNomCritere(),
                    null,
                    source,
                    valeur,
                    consoElecMoyenne
            );
        }

        public static ImpactReseau domain(Etape etape, Critere critere, String refReseau, String source, Double valeur, Double consoElecMoyenne) {
            return new ImpactReseau()
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setRefReseau(refReseau)
                    .setSource(source)
                    .setValeur(valeur)
                    .setConsoElecMoyenne(consoElecMoyenne);
        }

        public static ImpactReseauEntity entity(EtapeEntity etape, CritereEntity critere, String refReseau, String source, Double valeur, Double consoElecMoyenne) {
            return new ImpactReseauEntity()
                    .setEtape(etape.getCode())
                    .setCritere(critere.getNomCritere())
                    .setRefReseau(refReseau)
                    .setSource(source)
                    .setValeur(valeur)
                    .setConsoElecMoyenne(consoElecMoyenne);
        }

        public static ImpactReseauIdDTO idDTO(EtapeIdDTO etapeIdDTO, CritereIdDTO critereId, String refReseau) {
            return ImpactReseauIdDTO
                    .builder()
                    .etapeACV(etapeIdDTO.getCode())
                    .critere(critereId.getNomCritere())
                    .refReseau(refReseau)
                    .build();
        }

        public static ImpactReseauId idDomain(EtapeId etapeId, CritereId critereId, String refReseau) {
            return new ImpactReseauId()
                    .setEtape(etapeId.getCode())
                    .setCritere(critereId.getNomCritere())
                    .setRefReseau(refReseau);
        }

        public static ImpactReseauIdEntity idEntity(EtapeIdEntity etapeIdEntity, CritereIdEntity critereId, String refReseau) {
            return new ImpactReseauIdEntity()
                    .setEtape(etapeIdEntity.getCode())
                    .setCritere(critereId.getNomCritere())
                    .setRefReseau(refReseau);
        }
    }

    public static class ImpactMessagerieFactory {

        public static ImpactMessagerieDTO dto(CritereDTO critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return ImpactMessagerieDTO.builder()
                    .critere(critere.getNomCritere())
                    .source(source)
                    .constanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .constanteOrdonneeOrigine(constanteOrdonneeOrigine)
                    .build();
        }

        public static ImpactMessagerie domain(Critere critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return new ImpactMessagerie()
                    .setCritere(critere.getNomCritere())
                    .setSource(source)
                    .setConstanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .setConstanteOrdonneeOrigine(constanteOrdonneeOrigine);
        }

        public static ImpactMessagerieEntity entity(CritereEntity critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine, String source) {
            return new ImpactMessagerieEntity()
                    .setCritere(critere.getNomCritere())
                    .setSource(source)
                    .setConstanteCoefficientDirecteur(constanteCoefficientDirecteur)
                    .setConstanteOrdonneeOrigine(constanteOrdonneeOrigine);
        }
    }

    public static class CorrespondanceRefEquipementFactory {
        public static CorrespondanceRefEquipementDTO dto(String modeleSource, String refEquipementCible) {
            return CorrespondanceRefEquipementDTO.builder()
                    .modeleEquipementSource(modeleSource)
                    .refEquipementCible(refEquipementCible)
                    .build();
        }

        public static CorrespondanceRefEquipement domain(String modeleSource, String refEquipementCible) {
            return CorrespondanceRefEquipement.builder()
                    .modeleEquipementSource(modeleSource)
                    .refEquipementCible(refEquipementCible)
                    .build();
        }

        public static CorrespondanceRefEquipementEntity entity(String modeleSource, String refEquipementCible) {
            return new CorrespondanceRefEquipementEntity()
                    .setModeleEquipementSource(modeleSource)
                    .setRefEquipementCible(refEquipementCible);
        }
    }
}
