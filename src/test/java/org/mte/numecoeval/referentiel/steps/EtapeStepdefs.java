package org.mte.numecoeval.referentiel.steps;

import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Et;
import io.cucumber.java.fr.Quand;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportEtapePortImpl;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.etape.ReferentielAdministrationEtapeRestApi;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.etape.ReferentielEtapeRestApi;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EtapeStepdefs extends AbstractStepDefinitions {
    private List<EtapeDTO> etapeCVS;
    private EtapeDTO[] etapesAPI;

    @Quand("le fichier CSV {string} est injecte")
    public void leFichierCSVEstInjecte(String nomFichier) throws FileNotFoundException, NoSuchMethodException {
        setupRestAssured();
        File file = ResourceUtils.getFile("classpath:" + nomFichier);
        //stockage des dtos etapes pour verifications avec la ressource rest api apres alimentation
        ResultatImport<EtapeDTO> resultatImport = new ImportEtapePortImpl().importCSV(new FileInputStream(file));
        etapeCVS = resultatImport.getObjects();
        String requestPath = Arrays.stream(ReferentielAdministrationEtapeRestApi.class.getMethod("importCSV", MultipartFile.class)
                .getAnnotation(PostMapping.class).path()).findFirst().orElse(null);
        Response response = given()
                .multiPart("file", file)
                .post(requestPath)
                .thenReturn();
        //check que le status est OK
        assertEquals(200, response.getStatusCode());
    }

    @Alors("Il existe {int} elements dans le referentiel Etape ACV")
    public void ilExisteElementsDansLeReferentielEtapeACV(int taille) throws NoSuchMethodException {
        String requestPath = Arrays.stream(ReferentielEtapeRestApi.class.getMethod("getAll")
                .getAnnotation(GetMapping.class).path()).findFirst().orElse(null);
        Response response = given()
                .contentType(ContentType.JSON)
                .get(requestPath)
                .thenReturn();
        etapesAPI = response.as(EtapeDTO[].class);
        assertEquals(taille, response.as(List.class).size());


    }


    @Et("chacun des elements du referentiel ajouté est identique au element du fichier csv")
    public void chacunDesElementsDuReferentielAjoutéEstIdentiqueAuElementDuFichierCsv() {

        List<EtapeDTO> resultEgalite = etapeCVS.stream().filter(csv ->
                        Arrays.stream(etapesAPI).collect(Collectors.toList()).stream().anyMatch(api ->
                                csv.getLibelle().equals(api.getLibelle())
                                        && csv.getCode().equals(api.getCode())
                        ))
                .collect(Collectors.toList());

        assertEquals(etapeCVS.size(), resultEgalite.size());
    }
}
