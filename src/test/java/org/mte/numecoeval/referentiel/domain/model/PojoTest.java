package org.mte.numecoeval.referentiel.domain.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.meanbean.test.BeanTester;
import org.mte.numecoeval.referentiel.domain.model.id.CritereId;
import org.mte.numecoeval.referentiel.domain.model.id.EtapeId;
import org.mte.numecoeval.referentiel.domain.model.id.HypotheseId;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactReseauId;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.HypotheseEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.CritereIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.EtapeIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.HypotheseIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactEquipementIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

class PojoTest {

    private BeanTester tester;

    @BeforeEach
    public void init() {
        tester = new BeanTester();
        tester.setIterations(5);
        tester.getFactoryCollection().addFactory(LocalDate.class, LocalDate::now);
        tester.getFactoryCollection().addFactory(LocalDateTime.class, LocalDateTime::now);
        tester.getFactoryCollection().addFactory(OffsetDateTime.class, OffsetDateTime::now);
        tester.getFactoryCollection().addFactory(UUID.class, UUID::randomUUID);
    }

    @ParameterizedTest
    @ValueSource(classes = {
            Etape.class, Critere.class, Hypothese.class,
            ImpactEquipement.class, ImpactReseau.class, MixElectrique.class,
            EtapeId.class, CritereId.class, HypotheseId.class,
            ImpactEquipementId.class, ImpactReseauId.class, MixElectriqueId.class
    })
    @DisplayName("Tests des Getter/Setter des pojos de domain/model")
    void modelsGetterAndSetterTest(Class classToTest) {
        tester.testBean(classToTest);
    }

    @ParameterizedTest
    @ValueSource(classes = {
            Etape.class, Critere.class, Hypothese.class,
            ImpactEquipement.class, ImpactReseau.class, MixElectrique.class,
            EtapeId.class,CritereId.class, HypotheseId.class,
            ImpactEquipementId.class, ImpactReseauId.class, MixElectriqueId.class
    })
    @DisplayName("Tests des equals/hashCode des pojos de domain/model")
    void modelsEqualsAndHashCodeTest(Class classToTest) {
        EqualsVerifier.simple().forClass(classToTest).verify();
    }

    @ParameterizedTest
    @ValueSource(classes = {
            EtapeEntity.class, CritereEntity.class, HypotheseEntity.class,
            ImpactEquipementEntity.class, ImpactReseauEntity.class, MixElectriqueEntity.class,
            EtapeIdEntity.class, CritereIdEntity.class, HypotheseIdEntity.class,
            ImpactEquipementIdEntity.class, ImpactReseauIdEntity.class, MixElectriqueIdEntity.class
    })
    @DisplayName("Tests des Getter/Setter des entities")
    void entitiesGetterAndSetterTest(Class classToTest) {
        tester.testBean(classToTest);
    }

    @ParameterizedTest
    @ValueSource(classes = {
            EtapeIdEntity.class, CritereIdEntity.class, HypotheseIdEntity.class,
            ImpactEquipementIdEntity.class, ImpactReseauIdEntity.class, MixElectriqueIdEntity.class
    })
    @DisplayName("Tests des equals/hashCode des id d'entities")
    void entitiesIDEqualsAndHashCodeTest(Class classToTest) {
        EqualsVerifier.simple().forClass(classToTest).verify();
    }

    @ParameterizedTest
    @ValueSource(classes = {
            EtapeEntity.class, CritereEntity.class, HypotheseEntity.class,
            ImpactEquipementEntity.class, ImpactReseauEntity.class, MixElectriqueEntity.class,
    })
    @DisplayName("Tests des equals/hashCode des entities")
    void entitiesEqualsAndHashCodeTest(Class classToTest) {
        EqualsVerifier.simple()
                .suppress(Warning.SURROGATE_KEY) // à cause des @Id sur les champs
                .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED) // car par défaut on utilise tous les champs et pas seulements les champs marqués par @Id
                .forClass(classToTest).verify();
    }

}
