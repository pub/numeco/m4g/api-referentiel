package org.mte.numecoeval.referentiel.domain.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ReferentielExceptionTest {
    /**
     * Method under test: {@link ReferentielException#ReferentielException(String)}
     */
    @Test
    void testConstructor() {
        ReferentielException actualReferentielException = new ReferentielException("An error occurred");
        assertNull(actualReferentielException.getCause());
        assertEquals(0, actualReferentielException.getSuppressed().length);
        assertEquals("An error occurred", actualReferentielException.getMessage());
        assertEquals("An error occurred", actualReferentielException.getLocalizedMessage());
    }
}

