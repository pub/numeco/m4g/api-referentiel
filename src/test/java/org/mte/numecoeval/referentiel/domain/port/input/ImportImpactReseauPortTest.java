package org.mte.numecoeval.referentiel.domain.port.input;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactReseauPortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportImpactReseauPortTest {

    ImportCSVReferentielPort<ImpactReseauDTO> importPortToTest = new ImportImpactReseauPortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactreseau.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "DISTRIBUTION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.428).equals(dto.getValeur())
                        && Double.valueOf(0.0020).equals(dto.getConsoElecMoyenne())
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "FABRICATION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(518.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "FIN_DE_VIE".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(64.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "UTILISATION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(76.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactreseau_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(3, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne refReseau ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°5 est invalide : La colonne etapeACV ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°6 est invalide : La colonne critere ne peut être vide"::equals));
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "DISTRIBUTION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.428).equals(dto.getValeur())
                        && Double.valueOf(0.0020).equals(dto.getConsoElecMoyenne())
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "FABRICATION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(518.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "FIN_DE_VIE".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(64.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "impactReseauMobileMoyen".equals(dto.getRefReseau())
                        && "UTILISATION".equals(dto.getEtapeACV())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(76.28).equals(dto.getValeur())
                        && StringUtils.isEmpty(dto.getUnite())
                        && dto.getConsoElecMoyenne() == null
                        && "NegaOctet V1.00".equals(dto.getSource()))
        );
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
