package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactEquipementPortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportImpactEquipementPortTest {

    ImportCSVReferentielPort<ImpactEquipementDTO> importPortToTest = new ImportImpactEquipementPortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactEquipement.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "FABRICATION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(149.0).equals(dto.getValeur())
                        && Double.valueOf(30.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && Objects.isNull(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "DISTRIBUTION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(11.0082325332).equals(dto.getValeur())
                        && Double.valueOf(29.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && Objects.isNull(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "UTILISATION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.9485359999999998).equals(dto.getValeur())
                        && Double.valueOf(28.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && Objects.isNull(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "FIN_DE_VIE".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.46).equals(dto.getValeur())
                        && Double.valueOf(1.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && Objects.isNull(dto.getDescription())
                )
        );
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/impactEquipement_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(3, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne refEquipement ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°5 est invalide : La colonne etapeacv ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°6 est invalide : La colonne critere ne peut être vide"::equals));
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "FABRICATION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(149.0).equals(dto.getValeur())
                        && Double.valueOf(30.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && "description Ordinateur Portable 1".equals(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "DISTRIBUTION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(11.0082325332).equals(dto.getValeur())
                        && Double.valueOf(29.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && "description Ordinateur Portable 2".equals(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "UTILISATION".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.9485359999999998).equals(dto.getValeur())
                        && Double.valueOf(28.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && "description Ordinateur Portable 3".equals(dto.getDescription())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Ordinateur portable 14.5 8 Go RAM 564 Go SSD".equals(dto.getRefEquipement())
                        && "FIN_DE_VIE".equals(dto.getEtape())
                        && "Changement climatique".equals(dto.getCritere())
                        && Double.valueOf(1.46).equals(dto.getValeur())
                        && Double.valueOf(1.1).equals(dto.getConsoElecMoyenne())
                        && "Ref_Base_donnee_reference_20220728.xlsx".equals(dto.getSource())
                        && "Ordinateur Portable".equals(dto.getType())
                        && "description Ordinateur Portable 4".equals(dto.getDescription())
                )
        );
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
