package org.mte.numecoeval.referentiel.domain.exception;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ReferentielRuntimeExceptionTest {
    /**
     * Method under test: {@link ReferentielRuntimeException#ReferentielRuntimeException(String)}
     */
    @Test
    void testConstructor() {
        ReferentielRuntimeException actualReferentielRuntimeException = new ReferentielRuntimeException(
                "An error occurred");
        assertNull(actualReferentielRuntimeException.getCause());
        assertEquals(0, actualReferentielRuntimeException.getSuppressed().length);
        assertEquals("An error occurred", actualReferentielRuntimeException.getMessage());
        assertEquals("An error occurred", actualReferentielRuntimeException.getLocalizedMessage());
    }
}

