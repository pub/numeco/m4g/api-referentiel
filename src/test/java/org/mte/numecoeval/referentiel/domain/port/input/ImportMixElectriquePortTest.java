package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportMixElectriquePortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportMixElectriquePortTest {

    ImportCSVReferentielPort<MixElectriqueDTO> importPortToTest = new ImportMixElectriquePortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/mixElectrique.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(4, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "France".equals(dto.getPays())
                        && "FR".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(149).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "Brésil".equals(dto.getPays())
                        && "BR".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(11.0082325332).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "Angleterre".equals(dto.getPays())
                        && "EN".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(1.9485359999999998).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "USA".equals(dto.getPays())
                        && "US".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(1.46).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/mixElectrique_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(4, resultatImport.getNbrLignesImportees());
        assertEquals(3, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne pays ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°5 est invalide : La colonne raccourcisAnglais ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°6 est invalide : La colonne critere ne peut être vide"::equals));
        assertEquals(4, resultatImport.getObjects().size());
        resultatImport.getObjects().forEach(
                mixElectriqueDTO -> System.out.printf("%s ; %s ; %s ; %s%n", mixElectriqueDTO.getCritere(), mixElectriqueDTO.getPays(), mixElectriqueDTO.getRaccourcisAnglais(), mixElectriqueDTO.getValeur())
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "France".equals(dto.getPays())
                        && "FR".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(149).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "Brésil".equals(dto.getPays())
                        && "BR".equals(dto.getRaccourcisAnglais())
                        //&& Double.valueOf(11.0082325332).equals(dto.getValeur())
                        //&& "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource())
                )
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "Angleterre".equals(dto.getPays())
                        && "EN".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(1.9485359999999998).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
        assertTrue(resultatImport.getObjects().stream().anyMatch(dto ->
                "Changement climatique".equals(dto.getCritere())
                        && "USA".equals(dto.getPays())
                        && "US".equals(dto.getRaccourcisAnglais())
                        && Double.valueOf(1.46).equals(dto.getValeur())
                        && "Ref_Base_donnee_reference_20480728.xlsx".equals(dto.getSource()))
        );
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
