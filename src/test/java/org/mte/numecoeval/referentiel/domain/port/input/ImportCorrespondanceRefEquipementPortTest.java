package org.mte.numecoeval.referentiel.domain.port.input;

import org.junit.jupiter.api.Test;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportCorrespondanceRefEquipementPortImpl;
import org.springframework.util.ResourceUtils;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class ImportCorrespondanceRefEquipementPortTest {

    ImportCSVReferentielPort<CorrespondanceRefEquipementDTO> importPortToTest = new ImportCorrespondanceRefEquipementPortImpl();

    @Test
    void importCSV_shouldImportAllDatas() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/correspondanceRefEquipement.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(3, resultatImport.getNbrLignesImportees());
        assertEquals(0, resultatImport.getErreurs().size());
        assertEquals(3, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele01".equals(etapeDTO.getModeleEquipementSource()) && "refCible01".equals(etapeDTO.getRefEquipementCible())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele02".equals(etapeDTO.getModeleEquipementSource()) && "refCible01".equals(etapeDTO.getRefEquipementCible())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele03".equals(etapeDTO.getModeleEquipementSource()) && "refCible02".equals(etapeDTO.getRefEquipementCible())));
    }

    @Test
    void importCSV_whenErrorInMiddleOfFile_shouldImportDatasWithErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/correspondanceRefEquipement_errorInMiddle.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(3, resultatImport.getNbrLignesImportees());
        assertEquals(2, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°4 est invalide : La colonne modeleEquipementSource ne peut être vide"::equals));
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°5 est invalide : La colonne refEquipementCible ne peut être vide"::equals));
        assertEquals(3, resultatImport.getObjects().size());
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele01".equals(etapeDTO.getModeleEquipementSource()) && "refCible01".equals(etapeDTO.getRefEquipementCible())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele01".equals(etapeDTO.getModeleEquipementSource()) && "refCible01".equals(etapeDTO.getRefEquipementCible())));
        assertTrue(resultatImport.getObjects().stream().anyMatch(etapeDTO -> "modele03".equals(etapeDTO.getModeleEquipementSource()) && "refCible02".equals(etapeDTO.getRefEquipementCible())));
    }

    @Test
    void importCSV_whenWrongFile_shouldReturnOnlyErrors() throws Exception {
        File file = ResourceUtils.getFile("classpath:csv/unit/wrongCSVFile.csv");
        var resultatImport = importPortToTest.importCSV(new FileInputStream(file));

        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertTrue(resultatImport.getErreurs().stream().anyMatch("La ligne n°2 est invalide : Entêtes incohérentes"::equals));
    }

    @Test
    void importCSV_whenStreamAlreadyClosedShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doNothing().when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }

    @Test
    void importCSV_whenFileNotFoundShouldReturnReportWithOneError() throws IOException {
        DataInputStream dataInputStream = mock(DataInputStream.class);
        doThrow(new FileNotFoundException("start Read csv etape")).when(dataInputStream).close();

        var resultatImport = importPortToTest.importCSV(dataInputStream);

        verify(dataInputStream).close();
        assertEquals(0, resultatImport.getNbrLignesImportees());
        assertEquals(1, resultatImport.getErreurs().size());
        assertEquals("Le fichier CSV n'a pas pu être lu.", resultatImport.getErreurs().get(0));
    }
}
