package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactMessageriePortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactMessagerieRepository;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class ImpactMessagerieCsvExportServiceTest {

    @InjectMocks
    ImpactMessagerieCsvExportService exportService;

    @Mock
    ImpactMessagerieRepository repository;

    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        assertEquals(ImportImpactMessageriePortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {
        var entities = List.of(
                TestDataFactory.ImpactMessagerieFactory.entity(
                        TestDataFactory.CritereFactory.entity("Changement climatique", "",""),
                        0.2, 0.0005, "Test"
                ),
                TestDataFactory.ImpactMessagerieFactory.entity(
                        TestDataFactory.CritereFactory.entity("Acidification", "",""),
                        0.2, 0.0005, "Test"
                )
        );
        when(repository.findAll()).thenReturn(entities);

        assertEquals(entities, exportService.getObjectsToWrite());
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.ImpactMessagerieFactory.entity(
                TestDataFactory.CritereFactory.entity("Changement climatique", "",""),
                0.2, 0.0005, "Test"
        );
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, entity));

        Mockito.verify(csvPrinter, times(1)).printRecord(
                entity.getCritere(),
                df.format(entity.getConstanteCoefficientDirecteur()),
                df.format(entity.getConstanteOrdonneeOrigine()),
                entity.getSource());
    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord(){
        var entity = TestDataFactory.ImpactMessagerieFactory.entity(
                TestDataFactory.CritereFactory.entity("Changement climatique", "",""),
                0.2, 0.0005, "Test"
        );

        assertDoesNotThrow(() -> exportService.logRecordError(entity, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord(){
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile(){
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = List.of(
                TestDataFactory.ImpactMessagerieFactory.entity(
                        TestDataFactory.CritereFactory.entity("Changement climatique", "",""),
                        0.2, 0.0005, "Test"
                )
        );
        when(repository.findAll()).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                "critere;constanteCoefficientDirecteur;constanteOrdonneeOrigine;source\r\nChangement climatique;0.2;0.0005;Test\r\n",
                result
        );
    }
}
