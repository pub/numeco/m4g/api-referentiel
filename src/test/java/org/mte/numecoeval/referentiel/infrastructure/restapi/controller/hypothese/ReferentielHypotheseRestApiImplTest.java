package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.hypothese;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.HypotheseCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.HypotheseIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.HypotheseFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ReferentielHypotheseRestApiImpl.class})
@ExtendWith(SpringExtension.class)
class ReferentielHypotheseRestApiImplTest {
    @Autowired
    private ReferentielHypotheseRestApiImpl referentielRestApi;

    @MockBean
    private HypotheseFacade referentielFacade;

    @MockBean
    private HypotheseCsvExportService csvExportService;

    @Test
    void get_shouldCallFacadeGetAndReturnMatchingDTO() throws ReferentielException {
        String cle = "cle";
        HypotheseIdDTO idDTO = new HypotheseIdDTO().setCode(cle);
        var expectedDTO = HypotheseDTO.builder().code(cle).valeur("test").build();


        when(referentielFacade.get(idDTO)).thenReturn(expectedDTO);
        var receivedDTO = referentielRestApi.get(cle);
        assertSame(receivedDTO, receivedDTO);
        verify(referentielFacade).get(idDTO);
    }

    @Test
    void get_whenNotFound_thenShouldThrowReferentielException() throws ReferentielException {
        String cle = "cle";
        HypotheseIdDTO idDTO = new HypotheseIdDTO().setCode(cle);

        when(referentielFacade.get(idDTO)).thenThrow(new ReferentielException("Hypothèse non trouvé"));
        var exception = assertThrows(ReferentielException.class,() -> referentielRestApi.get(cle));
        assertEquals("Hypothèse non trouvé", exception.getMessage());
    }

    @Test
    void importCSV_shouldCallPurgeAndAddAll() throws IOException, ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        referentielRestApi.importCSV(new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        verify(referentielFacade).purgeAndAddAll(any());
    }

    @Test
    void importCSV_whenEmptyFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        MockMultipartFile file = new MockMultipartFile("Name", (byte[]) null);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(file));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void importCSV_whenNullFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(null));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void exportCSV_shouldReturnCSVFile() throws UnsupportedEncodingException {
        var servletResponse = new MockHttpServletResponse();

        assertDoesNotThrow(() -> referentielRestApi.exportCSV(servletResponse));

        assertEquals("text/csv;charset=UTF-8", servletResponse.getContentType());
        String headerContentDisposition = servletResponse.getHeader("Content-Disposition");
        assertNotNull(headerContentDisposition);
        assertTrue(headerContentDisposition.contains("attachment; filename=hypotheses-"));
        assertEquals(200, servletResponse.getStatus());

    }
}

