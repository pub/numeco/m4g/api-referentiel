package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeequipement;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.TypeEquipementCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.TypeEquipementFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ReferentielTypeEquipementRestApiImpl.class})
@ExtendWith(SpringExtension.class)
class ReferentielTypeEquipementRestApiImplTest {
    @Autowired
    private ReferentielTypeEquipementRestApiImpl referentielRestApi;

    @MockBean
    private TypeEquipementFacade referentielFacade;

    @MockBean
    private TypeEquipementCsvExportService csvExportService;

    @Test
    void getAll_shouldCallFacadeGetAllAndReturnAllDTOs() {
        ArrayList<TypeEquipementDTO> typeEquipementDTOS = new ArrayList<>();
        when(referentielFacade.getAllTypesEquipement()).thenReturn(typeEquipementDTOS);
        List<TypeEquipementDTO> actualAll = referentielRestApi.getTypesEquipement();
        assertSame(typeEquipementDTOS, actualAll);
        assertTrue(actualAll.isEmpty());
        verify(referentielFacade).getAllTypesEquipement();
    }

    @Test
    void getTypeEquipement_shouldCallFacadeGetAndReturnDTO() throws ReferentielException {
        String expectedType = "Switch";
        TypeEquipementDTO expectedDTO = TestDataFactory.TypeEquipementFactory.dto(expectedType, true, 1.0, "test", "test","test");
        when(referentielFacade.getTypeEquipementForType(expectedType)).thenReturn(expectedDTO);
        TypeEquipementDTO actualResponse = referentielRestApi.getTypeEquipement(expectedType);
        assertSame(expectedDTO, actualResponse);
        verify(referentielFacade).getTypeEquipementForType(expectedType);
    }

    @Test
    void importCSV_shouldCallPurgeAndAddAll() throws IOException, ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        referentielRestApi.importCSV(new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        verify(referentielFacade).purgeAndAddAll(any());
    }

    @Test
    void importCSV_whenEmptyFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        MockMultipartFile file = new MockMultipartFile("Name", (byte[]) null);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(file));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void importCSV_whenNullFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(null));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void exportCSV_shouldReturnCSVFile() {
        var servletResponse = new MockHttpServletResponse();

        assertDoesNotThrow(() -> referentielRestApi.exportCSV(servletResponse));

        assertEquals("text/csv;charset=UTF-8", servletResponse.getContentType());
        String headerContentDisposition = servletResponse.getHeader("Content-Disposition");
        assertNotNull(headerContentDisposition);
        assertTrue(headerContentDisposition.contains("attachment; filename=typeEquipement-"));
        assertEquals(200, servletResponse.getStatus());

    }
}
