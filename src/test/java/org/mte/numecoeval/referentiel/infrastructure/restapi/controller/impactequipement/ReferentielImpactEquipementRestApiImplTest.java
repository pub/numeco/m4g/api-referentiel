package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactequipement;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.ImpactEquipementCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.ImpactEquipementFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ReferentielImpactEquipementRestApiImpl.class})
@ExtendWith(SpringExtension.class)
class ReferentielImpactEquipementRestApiImplTest {
    @Autowired
    private ReferentielImpactEquipementRestApiImpl referentielRestApi;

    @MockBean
    private ImpactEquipementFacade referentielFacade;

    @MockBean
    private ImpactEquipementCsvExportService csvExportService;

    @Test
    void get_shouldCallFacadeGetAndReturnMatchingDTO() throws ReferentielException {
        String refEquipement = "Ordinateur Portable";
        String nomCritere = "Changement Climatique";
        String codeEtapeACV = "UTILISATION";
        var idDTO = new ImpactEquipementIdDTO(refEquipement,
                codeEtapeACV,
                nomCritere
        );
        var expectedDTO = ImpactEquipementDTO.builder()
                .etape(codeEtapeACV)
                .critere(nomCritere)
                .refEquipement(refEquipement)
                .source("Test")
                .description("Test")
                .valeur(1.0).build();

        when(referentielFacade.get(idDTO)).thenReturn(expectedDTO);

        var receivedDTO = referentielRestApi.get(refEquipement, nomCritere, codeEtapeACV);
        assertSame(receivedDTO, receivedDTO);
        verify(referentielFacade).get(idDTO);
    }

    @Test
    void get_whenNotFound_thenShouldThrowReferentielException() throws ReferentielException {
        String refEquipement = "Ordinateur Portable";
        String nomCritere = "Changement Climatique";
        String codeEtapeACV = "UTILISATION";
        var idDTO = new ImpactEquipementIdDTO(refEquipement,
                codeEtapeACV,
                nomCritere
        );

        when(referentielFacade.get(idDTO)).thenThrow(new ReferentielException("Impact Equipement non trouvé"));
        var exception = assertThrows(ReferentielException.class,() -> referentielRestApi.get(refEquipement, nomCritere, codeEtapeACV));
        assertEquals("Impact Equipement non trouvé", exception.getMessage());
    }

    @Test
    void importCSV_shouldCallPurgeAndAddAll() throws IOException, ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        referentielRestApi.importCSV(new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        verify(referentielFacade).purgeAndAddAll(any());
    }

    @Test
    void importCSV_whenEmptyFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        MockMultipartFile file = new MockMultipartFile("Name", (byte[]) null);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(file));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void importCSV_whenNullFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(null));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void exportCSV_shouldReturnCSVFile() throws UnsupportedEncodingException {
        var servletResponse = new MockHttpServletResponse();

        assertDoesNotThrow(() -> referentielRestApi.exportCSV(servletResponse));

        assertEquals("text/csv;charset=UTF-8", servletResponse.getContentType());
        String headerContentDisposition = servletResponse.getHeader("Content-Disposition");
        assertNotNull(headerContentDisposition);
        assertTrue(headerContentDisposition.contains("attachment; filename=impactEquipement-"));
        assertEquals(200, servletResponse.getStatus());

    }
}

