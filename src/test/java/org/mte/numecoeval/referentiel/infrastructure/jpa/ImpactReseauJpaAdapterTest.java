package org.mte.numecoeval.referentiel.infrastructure.jpa;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactReseau;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactReseauId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.adapter.ImpactReseauJpaAdapter;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactReseauRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactReseauMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ImpactReseauJpaAdapter.class})
@ExtendWith(SpringExtension.class)
class ImpactReseauJpaAdapterTest {
    @Autowired
    private ImpactReseauJpaAdapter impactReseauJpaAdapter;

    @MockBean
    private ImpactReseauMapper impactReseauMapper;

    @MockBean
    private ImpactReseauRepository impactReseauRepository;

    /**
     * Method under test: {@link ImpactReseauJpaAdapter#save(ImpactReseau)}
     */
    @Test
    void testSave() throws ReferentielException {
        String expectedCritere = "Nom Critere";
        String expectedEtape = "Code";
        ImpactReseau impactReseau = new ImpactReseau();
        impactReseau.setConsoElecMoyenne(10.0d);
        impactReseau.setCritere(expectedCritere);
        impactReseau.setEtape(expectedEtape);
        impactReseau.setRefReseau("Ref Reseau");
        impactReseau.setSource("Source");
        impactReseau.setValeur(10.0d);

        ImpactReseauEntity impactReseauEntity = new ImpactReseauEntity();
        impactReseauEntity.setConsoElecMoyenne(10.0d);
        impactReseauEntity.setCritere(expectedCritere);

        impactReseauEntity.setEtape(expectedEtape);
        impactReseauEntity.setRefReseau("Ref Reseau");
        impactReseauEntity.setSource("Source");
        impactReseauEntity.setValeur(10.0d);
        when(impactReseauMapper.toDomain((ImpactReseauEntity) any())).thenReturn(impactReseau);
        when(impactReseauMapper.toEntity((ImpactReseau) any())).thenReturn(impactReseauEntity);

        ImpactReseauEntity impactReseauEntity1 = new ImpactReseauEntity();
        impactReseauEntity1.setConsoElecMoyenne(10.0d);
        impactReseauEntity1.setCritere(expectedCritere);
        impactReseauEntity1.setEtape(expectedEtape);
        impactReseauEntity1.setRefReseau("Ref Reseau");
        impactReseauEntity1.setSource("Source");
        impactReseauEntity1.setValeur(10.0d);
        when(impactReseauRepository.save((ImpactReseauEntity) any())).thenReturn(impactReseauEntity1);

        ImpactReseau impactReseau1 = new ImpactReseau();
        impactReseau1.setConsoElecMoyenne(10.0d);
        impactReseau1.setCritere(expectedCritere);
        impactReseau1.setEtape(expectedEtape);
        impactReseau1.setRefReseau("Ref Reseau");
        impactReseau1.setSource("Source");
        impactReseau1.setValeur(10.0d);
        assertSame(impactReseau, impactReseauJpaAdapter.save(impactReseau1));
        verify(impactReseauMapper).toDomain((ImpactReseauEntity) any());
        verify(impactReseauMapper).toEntity((ImpactReseau) any());
        verify(impactReseauRepository).save((ImpactReseauEntity) any());
    }

    /**
     * Method under test: {@link ImpactReseauJpaAdapter#saveAll(Collection)}
     */
    @Test
    void testSaveAll() throws ReferentielException {
        when(impactReseauMapper.toEntity((Collection<ImpactReseau>) any())).thenReturn(new ArrayList<>());
        when(impactReseauRepository.saveAll((Iterable<ImpactReseauEntity>) any())).thenReturn(new ArrayList<>());
        impactReseauJpaAdapter.saveAll(new ArrayList<>());
        verify(impactReseauMapper).toEntity((Collection<ImpactReseau>) any());
        verify(impactReseauRepository).saveAll((Iterable<ImpactReseauEntity>) any());
    }

    /**
     * Method under test: {@link ImpactReseauJpaAdapter#get(ImpactReseauId)}
     */
    @Test
    void testGet() throws ReferentielException {
        String expectedCritere = "Nom Critere";
        String expectedEtape = "Code";
        ImpactReseauIdEntity impactReseauIdEntity = new ImpactReseauIdEntity();
        impactReseauIdEntity.setCritere(expectedCritere);
        impactReseauIdEntity.setEtape(expectedEtape);
        impactReseauIdEntity.setRefReseau("Ref Reseau");

        ImpactReseau impactReseau = new ImpactReseau();
        impactReseau.setConsoElecMoyenne(10.0d);
        impactReseau.setCritere(expectedCritere);
        impactReseau.setEtape(expectedEtape);
        impactReseau.setRefReseau("Ref Reseau");
        impactReseau.setSource("Source");
        impactReseau.setValeur(10.0d);
        when(impactReseauMapper.toDomain((ImpactReseauEntity) any())).thenReturn(impactReseau);
        when(impactReseauMapper.toEntityId((ImpactReseauId) any())).thenReturn(impactReseauIdEntity);

        ImpactReseauEntity impactReseauEntity = new ImpactReseauEntity();
        impactReseauEntity.setConsoElecMoyenne(10.0d);
        impactReseauEntity.setCritere(expectedCritere);
        impactReseauEntity.setEtape(expectedEtape);
        impactReseauEntity.setRefReseau("Ref Reseau");
        impactReseauEntity.setSource("Source");
        impactReseauEntity.setValeur(10.0d);
        Optional<ImpactReseauEntity> ofResult = Optional.of(impactReseauEntity);
        when(impactReseauRepository.findById((ImpactReseauIdEntity) any())).thenReturn(ofResult);

        ImpactReseauId impactReseauId = new ImpactReseauId();
        impactReseauId.setCritere(expectedCritere);
        impactReseauId.setEtape(expectedEtape);
        impactReseauId.setRefReseau("Ref Reseau");
        assertSame(impactReseau, impactReseauJpaAdapter.get(impactReseauId));
        verify(impactReseauMapper).toDomain((ImpactReseauEntity) any());
        verify(impactReseauMapper).toEntityId(any());
        verify(impactReseauRepository).findById(any());
    }

    /**
     * Method under test: {@link ImpactReseauJpaAdapter#get(ImpactReseauId)}
     */
    @Test
    void testGet2() throws ReferentielException {
        String expectedCritere = "Nom Critere";
        String expectedEtape = "Code";
        ImpactReseauIdEntity impactReseauIdEntity = new ImpactReseauIdEntity();
        impactReseauIdEntity.setCritere(expectedCritere);
        impactReseauIdEntity.setEtape(expectedEtape);
        impactReseauIdEntity.setRefReseau("Ref Reseau");

        ImpactReseau impactReseau = new ImpactReseau();
        impactReseau.setConsoElecMoyenne(10.0d);
        impactReseau.setCritere(expectedCritere);
        impactReseau.setEtape(expectedEtape);
        impactReseau.setRefReseau("Ref Reseau");
        impactReseau.setSource("Source");
        impactReseau.setValeur(10.0d);
        when(impactReseauMapper.toDomain((ImpactReseauEntity) any())).thenReturn(impactReseau);
        when(impactReseauMapper.toEntityId(any())).thenReturn(impactReseauIdEntity);
        when(impactReseauRepository.findById(any())).thenReturn(Optional.empty());

        ImpactReseauId impactReseauId = new ImpactReseauId();
        impactReseauId.setCritere(expectedCritere);
        impactReseauId.setEtape(expectedEtape);
        impactReseauId.setRefReseau("Ref Reseau");
        assertThrows(ReferentielException.class, () -> impactReseauJpaAdapter.get(impactReseauId));
        verify(impactReseauMapper).toEntityId((ImpactReseauId) any());
        verify(impactReseauRepository).findById((ImpactReseauIdEntity) any());
    }

    /**
     * Method under test: {@link ImpactReseauJpaAdapter#purge()}
     */
    @Test
    void testPurge() {
        doNothing().when(impactReseauRepository).deleteAll();
        impactReseauJpaAdapter.purge();
        verify(impactReseauRepository).deleteAll();
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>default or parameterless constructor of {@link ImpactReseauJpaAdapter}
     *   <li>{@link ImpactReseauJpaAdapter#getAll()}
     * </ul>
     */
    @Test
    void testConstructor() {
        Mockito.when(impactReseauRepository.findAll()).thenReturn(Collections.emptyList());
        assertTrue((impactReseauJpaAdapter.getAll().isEmpty()));
    }
}

