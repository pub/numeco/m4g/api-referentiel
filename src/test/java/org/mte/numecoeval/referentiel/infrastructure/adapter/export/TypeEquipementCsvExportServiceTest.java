package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeEquipementPortImpl;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeEquipementRepository;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

class TypeEquipementCsvExportServiceTest {

    @InjectMocks
    TypeEquipementCsvExportService exportService;

    @Mock
    TypeEquipementRepository repository;

    @Mock
    CSVPrinter csvPrinter;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getHeadersShouldReturnSameHeadersAsImport() {
        Assertions.assertEquals(ImportTypeEquipementPortImpl.getHeaders(), exportService.getHeaders());
    }

    @Test
    void getObjectsToWriteShouldReturnRepositoryFindAll() {
        var entities = List.of(
                TestDataFactory.TypeEquipementFactory.entity(
                        "Serveur", true, 6.0, "", "Test",
                        "test"),
                TestDataFactory.TypeEquipementFactory.entity(
                        "Ecran", false, 4.0, "", "Test",
                        "test")
        );
        when(repository.findAll()).thenReturn(entities);

        assertEquals(entities, exportService.getObjectsToWrite());
    }

    @Test
    void printRecordShouldUseEntityAttributes() throws IOException {
        var entity = TestDataFactory.TypeEquipementFactory.entity(
                "Serveur", true, 6.0, "", "Test",
                "test");
        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340);

        assertDoesNotThrow(() -> exportService.printRecord(csvPrinter, entity));

        Mockito.verify(csvPrinter, times(1)).printRecord(
                entity.getType(),
                entity.isServeur(),
                entity.getCommentaire(),
                df.format(entity.getDureeVieDefaut()),
                entity.getSource(),
                entity.getRefEquipementParDefaut());
    }

    @Test
    void logRecordErrorShouldLogSpecificErrorForRecord() {
        var entity = TestDataFactory.TypeEquipementFactory.entity(
                "Serveur", true, 6.0, "", "Test",
                "test");

        assertDoesNotThrow(() -> exportService.logRecordError(entity, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForRecord() {
        assertDoesNotThrow(() -> exportService.logRecordError(null, new Exception("Test")));
    }

    @Test
    void logRecordErrorShouldLogGenericErrorForFile() {
        assertDoesNotThrow(() -> exportService.logWriterError(new Exception("Test")));
    }

    @Test
    void writeToCsvShouldReturnCSV() {
        // given
        var entities = List.of(
                TestDataFactory.TypeEquipementFactory.entity(
                        "Serveur", true, 6.0, "Commentaires", "Test",
                        "test")
        );
        when(repository.findAll()).thenReturn(entities);
        StringWriter stringWriter = new StringWriter();

        // when
        exportService.writeToCsv(stringWriter);

        // Then
        String result = stringWriter.toString();
        assertEquals(
                "type;serveur;commentaire;dureeVieDefaut;source;refEquipementParDefaut\r\nServeur;true;Commentaires;6;Test;test\r\n",
                result
        );
    }
}
