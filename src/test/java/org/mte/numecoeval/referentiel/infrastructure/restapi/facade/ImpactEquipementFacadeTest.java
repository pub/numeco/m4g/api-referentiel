package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class ImpactEquipementFacadeTest {

    @InjectMocks
    ImpactEquipementFacade facadeToTest;

    @Mock
    private ReferentielPersistencePort<ImpactEquipement, ImpactEquipementId> persistencePort;

    private ImpactEquipementMapper mapper = new ImpactEquipementMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(facadeToTest, "mapper", mapper);
    }

    @Test
    void get_shouldReturnMatchingDTO() throws ReferentielException {
        var wantedDTOId = TestDataFactory.ImpactEquipementFactory.idDTO(
                TestDataFactory.DEFAULT_ETAPE,
                TestDataFactory.DEFAULT_CRITERE,
                "Ecran 27 pouces"
        );
        var expectedDomainID = TestDataFactory.ImpactEquipementFactory.idDomain(
                TestDataFactory.DEFAULT_ETAPE,
                TestDataFactory.DEFAULT_CRITERE,
                "Ecran 27 pouces"
        );
        var expectedDomain = TestDataFactory.ImpactEquipementFactory.domain(
                TestDataFactory.EtapeFactory.domain(TestDataFactory.DEFAULT_ETAPE, "Test"),
                TestDataFactory.CritereFactory.domain(TestDataFactory.DEFAULT_CRITERE, TestDataFactory.DEFAULT_UNITE, TestDataFactory.DEFAULT_CRITERE),
                "Ecran 27 pouces", "Test", "Monitor", 0.12, 0.0012
        );
        Mockito.when(persistencePort.get(expectedDomainID)).thenReturn(expectedDomain);

        var dto = assertDoesNotThrow(() -> facadeToTest.get(wantedDTOId));

        Assertions.assertEquals(expectedDomain.getEtape(), dto.getEtape());
        Assertions.assertEquals(expectedDomain.getCritere(), dto.getCritere());
        Assertions.assertEquals(expectedDomain.getRefEquipement(), dto.getRefEquipement());
        Assertions.assertEquals(expectedDomain.getType(), dto.getType());
        Assertions.assertEquals(expectedDomain.getSource(), dto.getSource());
        Assertions.assertEquals(expectedDomain.getValeur(), dto.getValeur());
        Assertions.assertEquals(expectedDomain.getConsoElecMoyenne(), dto.getConsoElecMoyenne());
    }

    @Test
    void purgeAndAddAll_ShouldCallPurgeThenSaveAll() throws ReferentielException {
        var dtosToSave = Arrays.asList(
                TestDataFactory.ImpactEquipementFactory.dto(
                        TestDataFactory.DEFAULT_ETAPE,
                        TestDataFactory.DEFAULT_CRITERE,
                        "Ecran 27 pouces", "Test", "Monitor", 0.12, 0.0012,
                        "test"),
                TestDataFactory.ImpactEquipementFactory.dto(
                        "FIN_DE_VIE",
                        "Acidification",
                        "Ecran 27 pouces", "Test", "Monitor", 0.12, 0.0012,
                        "test")
        );
        ArgumentCaptor<Collection<ImpactEquipement>> valueCapture = ArgumentCaptor.forClass(Collection.class);

        assertDoesNotThrow(() -> facadeToTest.purgeAndAddAll(dtosToSave));

        Mockito.verify(persistencePort, Mockito.times(1)).purge();
        Mockito.verify(persistencePort, Mockito.times(1)).saveAll(valueCapture.capture());

        var expectedDomains = valueCapture.getValue();
        assertNotNull(expectedDomains);
        assertEquals(dtosToSave.size(), expectedDomains.size());
        dtosToSave.forEach(dto -> {
            var matchingDomain = expectedDomains.stream()
                    .filter(domain ->
                            dto.getEtape().equals(domain.getEtape())
                            && dto.getCritere().equals(domain.getCritere())
                            && dto.getRefEquipement().equals(domain.getRefEquipement()))
                    .findAny();
            assertTrue(matchingDomain.isPresent());
            var domain = matchingDomain.get();
            assertEquals(dto.getEtape(), domain.getEtape());
            assertEquals(dto.getCritere(), domain.getCritere());
            assertEquals(dto.getRefEquipement(), domain.getRefEquipement());
            assertEquals(dto.getType(), domain.getType());
            assertEquals(dto.getSource(), domain.getSource());
            assertEquals(dto.getValeur(), domain.getValeur());
            assertEquals(dto.getConsoElecMoyenne(), domain.getConsoElecMoyenne());
        });
    }
}
