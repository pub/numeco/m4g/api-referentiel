package org.mte.numecoeval.referentiel.infrastructure.jpa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.adapter.ImpactEquipementJpaAdapter;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactEquipementRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ImpactEquipementJpaAdapterTest {

    @InjectMocks
    private ImpactEquipementJpaAdapter jpaAdapter;

    @Mock
    ImpactEquipementRepository repository;

    ImpactEquipementMapper mapper = new ImpactEquipementMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(jpaAdapter, "mapper", mapper);
    }

    @Test
    void get_shouldReturnDomain() {
        var expectedEntity = new ImpactEquipementEntity()
                .setEtape("UTILISATION")
                .setCritere("Changement climatique")
                .setRefEquipement("Ecran 27 pouces")
                .setType("Monitor")
                .setSource("Test")
                .setConsoElecMoyenne(0.020)
                .setValeur(0.120);

        var wantedId = new ImpactEquipementId()
                .setEtape("UTILISATION")
                .setCritere("Changement climatique")
                .setRefEquipement(expectedEntity.getRefEquipement());
        var wantedEntityId = mapper.toEntityId(wantedId);
        Mockito.when(repository.findById(wantedEntityId)).thenReturn(Optional.of(expectedEntity));

        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.get(wantedId) );

        assertNotNull(expectedDomain.getEtape());
        assertEquals(expectedEntity.getEtape(), expectedDomain.getEtape());
        assertNotNull(expectedDomain.getCritere());
        assertEquals(expectedEntity.getCritere(), expectedDomain.getCritere());
        assertEquals(expectedEntity.getRefEquipement(), expectedDomain.getRefEquipement());
        assertEquals(expectedEntity.getType(), expectedDomain.getType());
        assertEquals(expectedEntity.getConsoElecMoyenne(), expectedDomain.getConsoElecMoyenne());
        assertEquals(expectedEntity.getValeur(), expectedDomain.getValeur());
        assertEquals(expectedEntity.getSource(), expectedDomain.getSource());
    }

    @Test
    void get_shouldThrowException() {
        var wantedId = new ImpactEquipementId()
                .setEtape("Absent")
                .setCritere("Inexistant")
                .setRefEquipement("NonExistant");
        var wantedEntityId = mapper.toEntityId(wantedId);
        Mockito.when(repository.findById(wantedEntityId)).thenReturn(Optional.empty());

        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(wantedId) );

        assertEquals("Impact Equipement non trouvé",expectedException.getMessage());
    }

    @Test
    void get_whenNull_shouldThrowException() {
        ReferentielException expectedException = assertThrows(ReferentielException.class, () -> jpaAdapter.get(null) );

        assertEquals("Impact Equipement non trouvé",expectedException.getMessage());
    }

    @Test
    void purge_shouldCallDeleteAll() {
        jpaAdapter.purge();

        Mockito.verify(repository, Mockito.times(1)).deleteAll();
    }

    @Test
    void getAll_shouldCallfindAll() {
        jpaAdapter.getAll();

        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

    @Test
    void saveAll_shouldCallsaveAll() {
        var domainToSave = new ImpactEquipement()
                .setEtape("UTILISATION")
                .setCritere("Changement climatique")
                .setRefEquipement("Ecran 27 pouces")
                .setType("Monitor")
                .setSource("Test")
                .setConsoElecMoyenne(0.020)
                .setValeur(0.120);
        var entityToSave = mapper.toEntity(domainToSave);

        assertDoesNotThrow(() -> jpaAdapter.saveAll(Collections.singletonList(domainToSave)));

        Mockito.verify(repository, Mockito.times(1)).saveAll(Collections.singletonList(entityToSave));
    }

    @Test
    void save_shouldSaveAndReturnDomain() {
        var wantedDomain = new ImpactEquipement()
                .setEtape("UTILISATION")
                .setCritere("Changement climatique")
                .setRefEquipement("Ecran 27 pouces")
                .setType("Monitor")
                .setSource("Test")
                .setConsoElecMoyenne(0.020)
                .setValeur(0.120);
        var expectedEntity = mapper.toEntities(Collections.singletonList(wantedDomain)).get(0);

        Mockito.when(repository.save(expectedEntity)).thenReturn(expectedEntity);

        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.save(wantedDomain) );

        assertNotNull(expectedDomain.getEtape());
        assertEquals(expectedEntity.getEtape(), expectedDomain.getEtape());
        assertNotNull(expectedDomain.getCritere());
        assertEquals(expectedEntity.getCritere(), expectedDomain.getCritere());
        assertEquals(expectedEntity.getRefEquipement(), expectedDomain.getRefEquipement());
        assertEquals(expectedEntity.getType(), expectedDomain.getType());
        assertEquals(expectedEntity.getConsoElecMoyenne(), expectedDomain.getConsoElecMoyenne());
        assertEquals(expectedEntity.getValeur(), expectedDomain.getValeur());
        assertEquals(expectedEntity.getSource(), expectedDomain.getSource());
    }

    @ParameterizedTest
    @NullSource
    void save_shouldSaveAndReturnNull(ImpactEquipement nullValue) {
        var expectedDomain = assertDoesNotThrow( () -> jpaAdapter.save(nullValue) );

        assertNull(expectedDomain);
    }
}
