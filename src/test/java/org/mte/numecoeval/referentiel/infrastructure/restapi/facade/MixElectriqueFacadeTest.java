package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.factory.TestDataFactory;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapper;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapperImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class MixElectriqueFacadeTest {

    @InjectMocks
    MixElectriqueFacade facadeToTest;

    @Mock
    private ReferentielPersistencePort<MixElectrique, MixElectriqueId> persistencePort;

    private MixElectriqueMapper mapper = new MixElectriqueMapperImpl();

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ReflectionTestUtils.setField(facadeToTest, "mapper", mapper);
    }

    @Test
    void get_shouldReturnMatchingDTO() throws ReferentielException {
        var wantedDTOId = TestDataFactory.MixElectriqueFactory.idDTO(
                TestDataFactory.CritereFactory.idDTO(TestDataFactory.DEFAULT_CRITERE),
                "France"
        );
        var expectedDomainID = TestDataFactory.MixElectriqueFactory.idDomain(
                TestDataFactory.CritereFactory.idDomain(TestDataFactory.DEFAULT_CRITERE),
                "France"
        );
        var expectedDomain = TestDataFactory.MixElectriqueFactory.domain(
                TestDataFactory.CritereFactory.domain(TestDataFactory.DEFAULT_CRITERE, TestDataFactory.DEFAULT_UNITE, TestDataFactory.DEFAULT_CRITERE),
                "France", "FR", 1.0, "test"
        );

        Mockito.when(persistencePort.get(expectedDomainID)).thenReturn(expectedDomain);

        var result = assertDoesNotThrow(() -> facadeToTest.get(wantedDTOId));

        Assertions.assertEquals(expectedDomain.getCritere(), result.getCritere());
        Assertions.assertEquals(expectedDomain.getPays(), result.getPays());
        Assertions.assertEquals(expectedDomain.getRaccourcisAnglais(), result.getRaccourcisAnglais());
        Assertions.assertEquals(expectedDomain.getValeur(), result.getValeur());
        Assertions.assertEquals(expectedDomain.getSource(), result.getSource());
    }

    @Test
    void purgeAndAddAll_ShouldCallPurgeThenSaveAll() throws ReferentielException {
        var dtosToSave = Arrays.asList(
                TestDataFactory.MixElectriqueFactory.dto(
                        TestDataFactory.CritereFactory.dto(TestDataFactory.DEFAULT_CRITERE, TestDataFactory.DEFAULT_UNITE, TestDataFactory.DEFAULT_CRITERE),
                        "France", "FR", 1.0, "test"
                ),
                TestDataFactory.MixElectriqueFactory.dto(
                        TestDataFactory.CritereFactory.dto("Acidification", "m3", "Autre Test"),
                        "France", "FR", 1.0, "test"
                )
        );
        ArgumentCaptor<Collection<MixElectrique>> valueCapture = ArgumentCaptor.forClass(Collection.class);

        assertDoesNotThrow(() -> facadeToTest.purgeAndAddAll(dtosToSave));

        Mockito.verify(persistencePort, Mockito.times(1)).purge();
        Mockito.verify(persistencePort, Mockito.times(1)).saveAll(valueCapture.capture());

        var expectedDomains = valueCapture.getValue();
        assertNotNull(expectedDomains);
        assertEquals(dtosToSave.size(), expectedDomains.size());
        dtosToSave.forEach(dto -> {
            var matchingDomain = expectedDomains.stream()
                    .filter(domain -> dto.getCritere().equals(domain.getCritere())
                            && dto.getPays().equals(domain.getPays()))
                    .findAny();
            assertTrue(matchingDomain.isPresent());
            var domain = matchingDomain.get();
            assertEquals(dto.getCritere(), domain.getCritere());
            assertEquals(dto.getPays(), domain.getPays());
            assertEquals(dto.getRaccourcisAnglais(), domain.getRaccourcisAnglais());
            assertEquals(dto.getValeur(), domain.getValeur());
            assertEquals(dto.getSource(), domain.getSource());
        });
    }
}
