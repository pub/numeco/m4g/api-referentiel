package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.mixelectrique;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.MixElectriqueCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.MixElectriqueIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.MixElectriqueFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ReferentielMixElecRestApiImpl.class})
@ExtendWith(SpringExtension.class)
class ReferentielMixElectriqueRestApiImplTest {
    @Autowired
    private ReferentielMixElecRestApiImpl referentielRestApi;

    @MockBean
    private MixElectriqueFacade referentielFacade;

    @MockBean
    private MixElectriqueCsvExportService csvExportService;

    @Test
    void get_shouldCallFacadeGetAndReturnMatchingDTO() throws ReferentielException {
        String pays = "France";
        String nomCritere = "Changement Climatique";
        var idDTO = new MixElectriqueIdDTO(pays,
                nomCritere
        );
        var expectedDTO = MixElectriqueDTO.builder()
                .critere(nomCritere)
                .pays(pays)
                .source("Test")
                .valeur(1.0).build();

        when(referentielFacade.get(idDTO)).thenReturn(expectedDTO);

        var receivedDTO = referentielRestApi.get(pays, nomCritere);
        assertSame(receivedDTO, receivedDTO);
        verify(referentielFacade).get(idDTO);
    }

    @Test
    void get_whenNotFound_thenShouldThrowReferentielException() throws ReferentielException {
        String pays = "France";
        String nomCritere = "Changement Climatique";
        var idDTO = new MixElectriqueIdDTO(pays,
                nomCritere
        );

        when(referentielFacade.get(idDTO)).thenThrow(new ReferentielException("Mix Electrique non trouvé"));
        var exception = assertThrows(ReferentielException.class,() -> referentielRestApi.get(pays, nomCritere));
        assertEquals("Mix Electrique non trouvé", exception.getMessage());
    }

    @Test
    void importCSV_shouldCallPurgeAndAddAll() throws IOException, ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        referentielRestApi.importCSV(new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        verify(referentielFacade).purgeAndAddAll(any());
    }

    @Test
    void importCSV_whenEmptyFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        MockMultipartFile file = new MockMultipartFile("Name", (byte[]) null);
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(file));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void importCSV_whenNullFileThenShouldThrowException() throws ReferentielException {
        doNothing().when(referentielFacade).purgeAndAddAll(any());
        ResponseStatusException responseStatusException = assertThrows(ResponseStatusException.class, () -> referentielRestApi.importCSV(null));
        assertEquals(HttpStatus.BAD_REQUEST, responseStatusException.getStatusCode());
        assertEquals("Le fichier n'existe pas ou alors il est vide", responseStatusException.getReason());
    }

    @Test
    void exportCSV_shouldReturnCSVFile() {
        var servletResponse = new MockHttpServletResponse();

        assertDoesNotThrow(() -> referentielRestApi.exportCSV(servletResponse));

        assertEquals("text/csv;charset=UTF-8", servletResponse.getContentType());
        String headerContentDisposition = servletResponse.getHeader("Content-Disposition");
        assertNotNull(headerContentDisposition);
        assertTrue(headerContentDisposition.contains("attachment; filename=mixElec-"));
        assertEquals(200, servletResponse.getStatus());

    }
}

