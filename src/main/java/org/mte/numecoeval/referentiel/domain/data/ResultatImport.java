package org.mte.numecoeval.referentiel.domain.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ResultatImport<T> {

    List<String> erreurs = new ArrayList<>();

    long nbrLignesImportees = 0;

    List<T> objects;
}
