package org.mte.numecoeval.referentiel.domain.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TypeEquipement implements AbstractReferentiel {

    String type;
    boolean serveur;
    String commentaire;
    Double dureeVieDefaut;
    String source;
    // Référence de l'équipement par défaut, permet des correspondances en cas d'absence de correspondance direct.
    String refEquipementParDefaut;

}
