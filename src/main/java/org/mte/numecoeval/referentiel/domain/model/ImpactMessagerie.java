package org.mte.numecoeval.referentiel.domain.model;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class ImpactMessagerie implements AbstractReferentiel {

    String critere;
    Double constanteCoefficientDirecteur;
    Double constanteOrdonneeOrigine;
    String source;

}
