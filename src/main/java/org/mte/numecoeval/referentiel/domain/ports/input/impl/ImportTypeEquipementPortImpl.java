package org.mte.numecoeval.referentiel.domain.ports.input.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.BooleanUtils;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ImportTypeEquipementPortImpl implements ImportCSVReferentielPort<TypeEquipementDTO> {
    private static final String HEADER_TYPE = "type";
    private static final String[] HEADERS = new String[]{HEADER_TYPE,"serveur","commentaire","dureeVieDefaut","source","refEquipementParDefaut"};

    public static String[] getHeaders() {
        return HEADERS;
    }

    public void checkCSVRecord(CSVRecord csvRecord) throws ReferentielException {
        checkAllHeadersAreMapped(csvRecord, HEADERS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, HEADER_TYPE);
    }

    @Override
    public ResultatImport<TypeEquipementDTO> importCSV(InputStream csvInputStream) {
        ResultatImport<TypeEquipementDTO> resultatImport = new ResultatImport<>();
        List<TypeEquipementDTO> dtos = new ArrayList<>();

        try (Reader reader = new InputStreamReader(csvInputStream)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setDelimiter(CSV_SEPARATOR)
                    .setTrim(true)
                    .setAllowMissingColumnNames(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord -> {
                try {
                    checkCSVRecord(csvRecord);
                    dtos.add(TypeEquipementDTO.builder()
                                    .type(csvRecord.get(HEADER_TYPE).trim())
                                    .serveur(BooleanUtils.toBoolean(csvRecord.get("serveur").trim()))
                                    .commentaire(csvRecord.get("commentaire").trim())
                                    .dureeVieDefaut(getDoubleValueFromRecord(csvRecord,"dureeVieDefaut",null))
                                    .source(csvRecord.get("source").trim())
                                    .refEquipementParDefaut(getStringValueFromRecord(csvRecord, "refEquipementParDefaut"))
                            .build());
                }
                catch (Exception e) {
                    log.error("Erreur prévue lors de la lecture de la ligne {} : {}", csvRecord.getRecordNumber()+1, e.getMessage());
                    resultatImport.getErreurs().add( e.getMessage() );
                }
            });

        } catch (Exception e) {
            log.error("Erreur de traitement du fichier", e);

            resultatImport.setErreurs(Collections.singletonList("Le fichier CSV n'a pas pu être lu."));
            resultatImport.setNbrLignesImportees(0);
            resultatImport.setObjects(null);
            return resultatImport;
        }
        resultatImport.setObjects(dtos);
        resultatImport.setNbrLignesImportees(dtos.size());
        return resultatImport;
    }

}
