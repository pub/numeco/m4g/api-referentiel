package org.mte.numecoeval.referentiel.domain.ports.input.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.domain.data.ResultatImport;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.ImportCSVReferentielPort;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ImportMixElectriquePortImpl implements ImportCSVReferentielPort<MixElectriqueDTO> {
    private static final String HEADER_PAYS = "pays";
    private static final String HEADER_RACCOURCIS_ANGLAIS = "raccourcisAnglais";
    private static final String HEADER_CRITERE = "critere";
    private static final String[] HEADERS = new String[]{HEADER_PAYS, HEADER_RACCOURCIS_ANGLAIS, HEADER_CRITERE, "valeur", "source"};

    public static String[] getHeaders() {
        return HEADERS;
    }

    public void checkCSVRecord(CSVRecord csvRecord) throws ReferentielException {
        checkAllHeadersAreMapped(csvRecord, HEADERS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, HEADER_PAYS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, HEADER_RACCOURCIS_ANGLAIS);
        checkFieldIsMappedAndNotBlankInCSVRecord(csvRecord, HEADER_CRITERE);
    }

    @Override
    public ResultatImport<MixElectriqueDTO> importCSV(InputStream csvInputStream) {
        ResultatImport<MixElectriqueDTO> resultatImport = new ResultatImport<>();
        List<MixElectriqueDTO> dtos = new ArrayList<>();

        try (Reader reader = new InputStreamReader(csvInputStream)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.builder()
                    .setHeader()
                    .setDelimiter(CSV_SEPARATOR)
                    .setAllowMissingColumnNames(true)
                    .setTrim(true)
                    .setSkipHeaderRecord(true)
                    .build().parse(reader);
            records.forEach(csvRecord -> {
                try {
                    checkCSVRecord(csvRecord);
                    dtos.add(MixElectriqueDTO.builder()
                            .pays(csvRecord.get(HEADER_PAYS).trim())
                            .raccourcisAnglais(csvRecord.get(HEADER_RACCOURCIS_ANGLAIS).trim())
                            .critere(csvRecord.get(HEADER_CRITERE).trim())
                            .valeur(getDoubleValueFromRecord(csvRecord,"valeur",null))
                            .source(csvRecord.get("source"))
                            .build());
                }
                catch (Exception e) {
                    log.error("Erreur prévue lors de la lecture de la ligne {} : {}", csvRecord.getRecordNumber()+1, e.getMessage());
                    resultatImport.getErreurs().add( e.getMessage() );
                }
            });

        } catch (Exception e) {
            log.error("Erreur de traitement du fichier", e);

            resultatImport.setErreurs(Collections.singletonList("Le fichier CSV n'a pas pu être lu."));
            resultatImport.setNbrLignesImportees(0);
            resultatImport.setObjects(null);
            return resultatImport;
        }

        resultatImport.setObjects(dtos);
        resultatImport.setNbrLignesImportees(dtos.size());

        return resultatImport;
    }
}
