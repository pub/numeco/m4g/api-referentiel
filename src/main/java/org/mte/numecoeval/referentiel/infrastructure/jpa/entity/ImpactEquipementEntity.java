package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactEquipementIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@IdClass(ImpactEquipementIdEntity.class)
@Table(name = "REF_IMPACTEQUIPEMENT")
public class ImpactEquipementEntity implements AbstractReferentielEntity {
    @Id
    @Column(name = "REFEQUIPEMENT")
    String refEquipement;
    @Id
    @Column(name = "ETAPEACV")
    String etape;
    @Id
    @Column(name = "NOMCRITERE")
    String critere;
    String source;
    String type;
    Double valeur;
    Double consoElecMoyenne;

    String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImpactEquipementEntity that = (ImpactEquipementEntity) o;

        return new EqualsBuilder()
                .append(refEquipement, that.refEquipement)
                .append(etape, that.etape)
                .append(critere, that.critere)
                .append(source, that.source)
                .append(type, that.type)
                .append(valeur, that.valeur)
                .append(consoElecMoyenne, that.consoElecMoyenne)
                .append(description, that.description)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(refEquipement)
                .append(etape)
                .append(critere)
                .append(source)
                .append(type)
                .append(valeur)
                .append(consoElecMoyenne)
                .append(description)
                .toHashCode();
    }
}
