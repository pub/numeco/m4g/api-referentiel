package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactreseau;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ReferentielImpactReseauRestApi {

    @Operation(summary = "Endpoint interne à NumEcoEval - Récupération d'un Impact Réseau",
            description = """
                    Endpoint interne utilisé dans la génération des indicateurs par le module api-calcul de NumEcoEval.
                    Récupération d'un impact écologique vis à vis de l'usage du réseau en fonction de 3 paramètres:
                    <ul>
                        <li>La référence d'impact réseau: refReseau</li>
                        <li>Le critère d'impact: critere</li>
                        <li>L'étape ACV: etapeACV</li>
                    </ul>
                    .
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getImpactReseau")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "impact reseau trouvé",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ImpactReseauDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Impact Reseau  non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/impactreseaux", produces = MediaType.APPLICATION_JSON_VALUE)
    ImpactReseauDTO get(
            @RequestParam
            @Schema(description = "Référence de réseau recherché") final String refReseau,
            @RequestParam
            @Schema(description = "Nom du critère d'impact écologique") final String critere,
            @RequestParam
            @Schema(description = "Code de l'étape ACV") final String etapeacv
    );

}
