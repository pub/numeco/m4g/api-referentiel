package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactreseau;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReferentielAdministrationImpactReseauRestApi {

    @Operation(
            summary = "Exposition ressource ajout d' un Impact Reseau",
            tags = {"Administration détaillée"},
            hidden = true,
            description = "Exposition ressource ajout d' un Impact Reseau"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "impact reseau trouvé",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ImpactReseauDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Impact Réseau non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/impactreseaux", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ImpactReseauDTO> add(@RequestBody final ImpactReseauDTO impactReseauDTO) throws ReferentielException;

    @Operation(
            summary = "Exposition ressource update Impact Reseau  ",
            tags = {"Administration détaillée"},hidden = true
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Modication d'un impact reseau : les champs refReseau, critere & etapeACV ne peuvent pas être modifié",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = ImpactReseauDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Impact Réseau non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PutMapping(path = "/referentiel/impactreseaux", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ImpactReseauDTO> update(@RequestBody final ImpactReseauDTO impactReseauDTO) throws ReferentielException;


    @Operation(
            summary = "Alimentation du référentiel ImpactReseau par csv : annule et remplace.",
            tags = {"Import Référentiels"},
            operationId = "importImpactReseauxCSV"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rapport d'import du fichier CSV"),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/impactreseaux/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RapportImportDTO importCSV(@RequestPart("file") MultipartFile file) throws IOException, ReferentielException;


    @Operation(
            summary = "Exporter les impacts réseaux sous format csv",
            tags = {"Export Référentiels"},
            operationId = "exportImpactReseauxCSV"
    )
    @GetMapping("/referentiel/impactreseaux/csv")
    void exportCSV(HttpServletResponse servletResponse) throws IOException;


}
