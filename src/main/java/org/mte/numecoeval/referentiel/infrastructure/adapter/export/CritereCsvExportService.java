package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportCriterePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CritereEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.CritereRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CritereCsvExportService extends ReferentielCsvExportService<CritereEntity> {

    private final CritereRepository repository;

    public CritereCsvExportService(CritereRepository repository) {
        super(CritereEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportCriterePortImpl.getHeaders();
    }

    @Override
    public List<CritereEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(CritereEntity object) {
        return object.getNomCritere();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, CritereEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getNomCritere(),objectToWrite.getDescription(),objectToWrite.getUnite());
    }

}
