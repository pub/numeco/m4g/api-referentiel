package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "MixElectrique" , itemResourceRel = "MixElectriques")
@Tag(name = "MixElectrique - CRUD/Spring Data REST")
public interface MixElectriqueRepository extends JpaRepository<MixElectriqueEntity, MixElectriqueIdEntity> {

}
