package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactReseauPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactReseauRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ImpactReseauCsvExportService extends ReferentielCsvExportService<ImpactReseauEntity> {

    private final ImpactReseauRepository repository;

    public ImpactReseauCsvExportService(ImpactReseauRepository repository) {
        super(ImpactReseauEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportImpactReseauPortImpl.getHeaders();
    }

    @Override
    public List<ImpactReseauEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(ImpactReseauEntity object) {
        return String.join("-", object.getRefReseau(),object.getEtape(),object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, ImpactReseauEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getRefReseau(),
                objectToWrite.getEtape(),objectToWrite.getCritere(),
                formatDouble(objectToWrite.getValeur()),
                formatDouble(objectToWrite.getConsoElecMoyenne()),
                objectToWrite.getSource()
                );
    }

}
