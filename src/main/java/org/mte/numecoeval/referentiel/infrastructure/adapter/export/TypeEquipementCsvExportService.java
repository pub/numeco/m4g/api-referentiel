package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportTypeEquipementPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeEquipementRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class TypeEquipementCsvExportService extends ReferentielCsvExportService<TypeEquipementEntity> {

    private final TypeEquipementRepository repository;

    public TypeEquipementCsvExportService(TypeEquipementRepository repository) {
        super(TypeEquipementEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportTypeEquipementPortImpl.getHeaders();
    }

    @Override
    public List<TypeEquipementEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(TypeEquipementEntity object) {
        return object.getType();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, TypeEquipementEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getType(),
                objectToWrite.isServeur(),
                objectToWrite.getCommentaire(),
                formatDouble(objectToWrite.getDureeVieDefaut()),
                objectToWrite.getSource(), objectToWrite.getRefEquipementParDefaut());
    }

}
