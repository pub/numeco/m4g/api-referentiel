package org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
public class ImpactReseauIdDTO implements Serializable {
    @JsonProperty("refReseau")
    String refReseau;
    @JsonProperty("etapeACV")
    String etapeACV;
    @JsonProperty("critere")
    String critere;
}
