package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactMessageriePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactMessagerieRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ImpactMessagerieCsvExportService extends ReferentielCsvExportService<ImpactMessagerieEntity> {

    private final ImpactMessagerieRepository repository;

    public ImpactMessagerieCsvExportService(ImpactMessagerieRepository repository) {
        super(ImpactMessagerieEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportImpactMessageriePortImpl.getHeaders();
    }

    @Override
    public List<ImpactMessagerieEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(ImpactMessagerieEntity object) {
        return object.getCritere();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, ImpactMessagerieEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(
                objectToWrite.getCritere(),
                formatDouble(objectToWrite.getConstanteCoefficientDirecteur()),
                formatDouble(objectToWrite.getConstanteOrdonneeOrigine()),
                objectToWrite.getSource()
                );
    }

}
