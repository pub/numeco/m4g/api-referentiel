package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeEquipementEntity;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface TypeEquipementMapper {

    TypeEquipement toDomaine(TypeEquipementEntity typeEquipementEntity);
    TypeEquipement toDomaine(TypeEquipementDTO typeEquipementDTO);

    TypeEquipementEntity toEntity(TypeEquipement typeEquipement);
    List<TypeEquipementEntity> toEntities(Collection<TypeEquipement> typeEquipements);

    TypeEquipementDTO toDto(TypeEquipement typeEquipement);


}
