package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.referentiel.api.dto.TypeEquipementDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TypeEquipementFacade {
    private ReferentielPersistencePort<TypeEquipement,String> persistencePort;
    private TypeEquipementMapper mapper;

    public TypeEquipementFacade(ReferentielPersistencePort<TypeEquipement, String> persistencePort, TypeEquipementMapper mapper) {
        this.persistencePort = persistencePort;
        this.mapper = mapper;
    }

    public TypeEquipementDTO getTypeEquipementForType(String type) throws ReferentielException {
        var typeEquipement = persistencePort.get(type);
        if(typeEquipement!=null){
            return mapper.toDto(typeEquipement);
        }
        return null;
    }
    public List<TypeEquipementDTO>  getAllTypesEquipement(){
        List<TypeEquipement> typeEquipements = persistencePort.getAll();
        return CollectionUtils.emptyIfNull(typeEquipements).stream().map(typeEquipement -> mapper.toDto(typeEquipement)).toList();
    }

    public void purgeAndAddAll(List<TypeEquipementDTO> types)throws ReferentielException{
        persistencePort.purge();
        persistencePort.saveAll(CollectionUtils.emptyIfNull(types).stream()
                .map(typeEquipementDTO -> mapper.toDomaine(typeEquipementDTO))
                .toList());
    }

}
