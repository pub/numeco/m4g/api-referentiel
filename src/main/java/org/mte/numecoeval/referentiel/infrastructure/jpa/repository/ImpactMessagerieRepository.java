package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactMessagerieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "ImpactMessagerie" , itemResourceRel = "ImpactMessageries")
@Tag(name = "ImpactMessagerie - CRUD/Spring Data REST")
public interface ImpactMessagerieRepository extends JpaRepository<ImpactMessagerieEntity, String> {
}
