package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactReseau;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactReseauId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactReseauMapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactReseauIdDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ImpactReseauFacade {

    private ReferentielPersistencePort<ImpactReseau, ImpactReseauId> persistencePort;

    private ImpactReseauMapper mapper;

    public ImpactReseauDTO get(ImpactReseauIdDTO idImpactReseauDTO) throws ReferentielException {
        ImpactReseau impactReseau = persistencePort.get(mapper.toDomainId(idImpactReseauDTO));
        return mapper.toDTO(impactReseau);
    }

    public ImpactReseauDTO addOrUpdate(ImpactReseauDTO impactReseauDTO) throws ReferentielException {
        ImpactReseau save = persistencePort.save(mapper.toDomain(impactReseauDTO));
        return mapper.toDTO(save);
    }

    public void purgeAndAddAll(List<ImpactReseauDTO> impactesReseaux) throws ReferentielException {
        persistencePort.purge();
        List<ImpactReseau> impactReseauList = mapper.toDomainsFromDTO(impactesReseaux);
        persistencePort.saveAll(impactReseauList);
    }
}
