package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactequipement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReferentielAdministrationImpactEquipementRestApi {

    @Operation(
            summary = "Alimentation du référentiel ImpactEquipement par csv : annule et remplace.",
            description = "Alimentation du référentiel ImpactEquipement par csv : annule et remplace.",
            tags = {"Import Référentiels"},
            operationId = "importImpactEquipementCSV"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rapport d'import du fichier CSV"),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/impactequipements/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RapportImportDTO importCSV(@RequestPart("file") MultipartFile file) throws IOException, ReferentielException;


    @Operation(
            summary = "Exporter les impacts d'équipements sous format csv",
            tags = {"Export Référentiels"},
            operationId = "exportImpactEquipementCSV"
    )
    @GetMapping("/referentiel/impactequipements/csv")
    void exportCSV(HttpServletResponse servletResponse) throws IOException;

}
