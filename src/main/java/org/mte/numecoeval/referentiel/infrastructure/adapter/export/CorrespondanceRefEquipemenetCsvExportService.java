package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportHypothesePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.CorrespondanceRefEquipementRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CorrespondanceRefEquipemenetCsvExportService extends ReferentielCsvExportService<CorrespondanceRefEquipementEntity> {

    private final CorrespondanceRefEquipementRepository repository;

    public CorrespondanceRefEquipemenetCsvExportService(CorrespondanceRefEquipementRepository repository) {
        super(CorrespondanceRefEquipementEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportHypothesePortImpl.getHeaders();
    }

    @Override
    public List<CorrespondanceRefEquipementEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(CorrespondanceRefEquipementEntity object) {
        return object.getModeleEquipementSource();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, CorrespondanceRefEquipementEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getModeleEquipementSource(),objectToWrite.getRefEquipementCible());
    }

}
