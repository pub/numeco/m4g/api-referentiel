package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.mixelectrique;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ReferentielMixElectriqueRestApi {

    @Operation(summary = "Endpoint interne à NumEcoEval - Récupération d'un Mix électrique",
            description = """
                    Endpoint interne utilisé dans la génération des indicateurs par le module api-calcul de NumEcoEval.
                    Récupération d'un mix électrique en fonction de paramètres:
                    <ul>
                        <li>Le pays de l'équipement: pays</li>
                        <li>Le critère d'impact: critere</li>
                    </ul>
                    .
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getMixElectrique")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "mix Electrique trouvé",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = MixElectriqueDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "mix Electrique  non trouvé", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/mixelecs", produces = MediaType.APPLICATION_JSON_VALUE)
    MixElectriqueDTO get(
            @RequestParam
            @Schema(description = "Pays recherché") final String pays,
            @RequestParam
            @Schema(description = "Nom du critère d'impact écologique") final String critere
    );

}
