package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Etape;
import org.mte.numecoeval.referentiel.domain.model.id.EtapeId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.EtapeRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.EtapeMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class EtapeJpaAdapter implements ReferentielPersistencePort<Etape, EtapeId> {
    EtapeRepository etapeRepository;

    EtapeMapper etapeMapper;

    // Pour les purges des données filles
    ImpactReseauJpaAdapter impactReseauJpaAdapter;
    ImpactEquipementJpaAdapter impactEquipementJpaAdapter;


    @Override
    public Etape save(Etape referentiel) throws ReferentielException {
        return null;
    }

    @Override
    public void saveAll(Collection<Etape> referentiel) throws ReferentielException {
        etapeRepository.saveAll(etapeMapper.toEntities(referentiel));
    }

    @Override
    public Etape get(EtapeId id) throws ReferentielException {
        return null;
    }

    @Override
    public void purge() {
        // Purge des données parentes
        etapeRepository.deleteAll();
    }

    @Override
    public List<Etape> getAll() {
        return etapeMapper.toDomains(etapeRepository.findAll());
    }
}
