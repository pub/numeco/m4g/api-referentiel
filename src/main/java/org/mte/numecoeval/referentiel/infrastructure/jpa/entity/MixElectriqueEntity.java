package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@IdClass(MixElectriqueIdEntity.class)
@Table(name = "REF_MIXELEC")
public class MixElectriqueEntity implements AbstractReferentielEntity {

    @Id
    String pays;
    @Id
    @Column(name = "NOMCRITERE")
    String critere;
    @Column(name = "RACCOURCISANGLAIS")
    String raccourcisAnglais;
    String source;
    Double valeur;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MixElectriqueEntity that = (MixElectriqueEntity) o;

        return new EqualsBuilder().append(pays, that.pays).append(critere, that.critere).append(raccourcisAnglais, that.raccourcisAnglais).append(source, that.source).append(valeur, that.valeur).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(pays).append(critere).append(raccourcisAnglais).append(source).append(valeur).toHashCode();
    }
}
