package org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor

public class ImpactEquipementIdEntity implements AbstractReferentieIdEntity {
    String refEquipement;
    String etape;
    String critere;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImpactEquipementIdEntity that = (ImpactEquipementIdEntity) o;

        return new EqualsBuilder().append(refEquipement, that.refEquipement).append(etape, that.etape).append(critere, that.critere).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(refEquipement).append(etape).append(critere).toHashCode();
    }
}
