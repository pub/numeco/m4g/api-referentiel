package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.typeequipement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletResponse;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ReferentielAdministrationTypeEquipementRestApi {

    @Operation(
            summary = "Alimentation du référentiel Type Equipement par csv : annule et remplace.",
            tags = {"Import Référentiels"},
            operationId = "importTypeEquipementCSV"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rapport d'import du fichier CSV"),
            @ApiResponse(responseCode = "400", description = "Invalid request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @PostMapping(path = "/referentiel/typeEquipement/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    RapportImportDTO importCSV(@RequestPart("file") MultipartFile file) throws IOException, ReferentielException;


    @Operation(
            summary = "Exporter les impacts des types d'équipements sous format csv",
            tags = {"Export Référentiels"},
            operationId = "exportTypeEquipementCSV"
    )
    @GetMapping("/referentiel/typeEquipement/csv")
    void exportCSV(HttpServletResponse servletResponse) throws IOException;
}
