package org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id;

import java.io.Serializable;

public interface AbstractReferentieIdEntity extends Serializable {
    // Actuellement l'interface n'a pas de comportement par défaut ni de champ partagé
}
