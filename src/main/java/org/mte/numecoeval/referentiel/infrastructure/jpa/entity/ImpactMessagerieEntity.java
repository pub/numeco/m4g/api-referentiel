package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "REF_IMPACT_MESSAGERIE")
public class ImpactMessagerieEntity implements AbstractReferentielEntity {

    @Id
    @Column(name = "NOM_CRITERE")
    String critere;

    Double constanteCoefficientDirecteur;
    Double constanteOrdonneeOrigine;
    String source;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImpactMessagerieEntity that = (ImpactMessagerieEntity) o;

        return new EqualsBuilder().append(critere, that.critere).append(constanteCoefficientDirecteur, that.constanteCoefficientDirecteur).append(constanteOrdonneeOrigine, that.constanteOrdonneeOrigine).append(source, that.source).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(critere).append(constanteCoefficientDirecteur).append(constanteOrdonneeOrigine).append(source).toHashCode();
    }
}
