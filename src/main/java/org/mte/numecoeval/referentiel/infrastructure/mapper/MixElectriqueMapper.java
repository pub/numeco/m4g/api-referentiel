package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.MixElectriqueDTO;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.MixElectriqueIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface MixElectriqueMapper {

    MixElectriqueId toDomainId(MixElectriqueIdDTO id);

    MixElectriqueEntity toEntity(MixElectrique mixElecs);

    List<MixElectriqueEntity> toEntities(Collection<MixElectrique> mixElecs);

    MixElectriqueIdEntity toEntityId(MixElectriqueId id);

    MixElectrique toDomain(MixElectriqueEntity mixElec);

    List<MixElectrique> toDomains(List<MixElectriqueEntity> mixElec);

    List<MixElectrique> toDomainsFromDTO(List<MixElectriqueDTO> mixElecs);

    MixElectriqueDTO toDTO(MixElectrique mixElectrique);
}
