package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Builder
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "REF_TYPE_EQUIPEMENT")
public class TypeEquipementEntity implements AbstractReferentielEntity {
    @Id
    String type;
    boolean serveur;
    String commentaire;
    Double dureeVieDefaut;
    String source;

    // Référence de l'équipement par défaut, permet des correspondances en cas d'absence de correspondance direct.
    String refEquipementParDefaut;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TypeEquipementEntity that = (TypeEquipementEntity) o;

        return new EqualsBuilder().append(serveur, that.serveur).append(type, that.type).append(commentaire, that.commentaire).append(dureeVieDefaut, that.dureeVieDefaut).append(source, that.source).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(type).append(serveur).append(commentaire).append(dureeVieDefaut).append(source).toHashCode();
    }
}
