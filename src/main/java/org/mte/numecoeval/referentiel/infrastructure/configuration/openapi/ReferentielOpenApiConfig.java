package org.mte.numecoeval.referentiel.infrastructure.configuration.openapi;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReferentielOpenApiConfig {

    @Bean
    public OpenAPI configOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("API des référentiels de NumEcoEval")
                        .description("""
                                Endpoints permettant de manipuler les référentiels de NumEcoEval.
                                Les endpoints CRUD sont générés via Spring DataRest.
                                
                                Les endpoints d'export CSV permettent d'exporter l'intégralité d'un référentiel
                                sous forme de fichier CSV ré-importable via les endpoints d'imports.
                                
                                Les endpoints d'import fonctionnent en annule & remplace et supprimeront l'intégralité
                                du référentiel et utiliseront le contenu du CSV pour peupler le référentiel.
                                
                                Les endpoints internes sont utilisés par les différents modules de NumEcoEval.
                                """)
                        .version("v0.0.1")
                        .license(new License()
                                .name("Apache 2.0")
                                .url("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-referentiel/-/blob/main/LICENSE.txt")
                        )
                )
                .externalDocs(new ExternalDocumentation()
                        .description("NumEcoEval Documentation")
                        .url("https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/docs"));
    }

    @Bean
    public GroupedOpenApi springDataRestOpenApiGroup() {
        String[] paths = {
                "/CorrespondanceRefEquipement/**",
                "/Critere/**",
                "/Etape/**",
                "/Hypothèses/**",
                "/ImpactEquipement/**",
                "/ImpactMessagerie/**",
                "/ImpactReseau/**",
                "/MixElectrique/**",
                "/TypeEquipement/**",
                "/profile/**"
        };
        return GroupedOpenApi.builder()
                .group("spring-data-rest")
                .displayName("Spring Data Rest - Auto-Générés")
                .pathsToMatch(paths)
                .build();
    }

    @Bean
    public GroupedOpenApi numEcoEvalGlobalOpenApiGroup() {
        String[] paths = {
                "/referentiel/**"
        };
        return GroupedOpenApi.builder()
                .group("NumEcoEval-Global")
                .displayName("NumEcoEval Global")
                .pathsToMatch(paths)
                .build();
    }

    @Bean
    public GroupedOpenApi numEcoEvalOpenApiGroup() {
        String[] paths = {
                "/referentiel/*",
                "/referentiel/typesEquipement/{type}",
                "/referentiel/impactsMessagerie/{critere}",
        };
        return GroupedOpenApi.builder()
                .group("NumEcoEval")
                .displayName("NumEcoEval Internes")
                .pathsToMatch(paths)
                .build();
    }

    @Bean
    public GroupedOpenApi numEcoEvalAdministrationOpenApiGroup() {
        String[] paths = {
                "/referentiel/*/csv"
        };
        return GroupedOpenApi.builder()
                .group("NumEcoEval-administration")
                .displayName("Administration NumEcoEval")
                .pathsToMatch(paths)
                .build();
    }

}

