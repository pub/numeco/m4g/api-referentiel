package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.TypeEquipementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "TypeEquipement" , itemResourceRel = "TypesEquipements")
@Tag(name = "TypeEquipement - CRUD/Spring Data REST")
public interface TypeEquipementRepository extends JpaRepository<TypeEquipementEntity,String> {
}
