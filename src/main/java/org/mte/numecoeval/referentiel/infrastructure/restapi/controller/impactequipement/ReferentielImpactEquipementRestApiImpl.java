package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.impactequipement;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactEquipementPortImpl;
import org.mte.numecoeval.referentiel.infrastructure.adapter.export.ImpactEquipementCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.controller.BaseExportReferentiel;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.RapportImportDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;
import org.mte.numecoeval.referentiel.infrastructure.restapi.facade.ImpactEquipementFacade;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@Slf4j
@AllArgsConstructor
public class ReferentielImpactEquipementRestApiImpl implements BaseExportReferentiel<ImpactEquipementEntity>,ReferentielImpactEquipementRestApi, ReferentielAdministrationImpactEquipementRestApi {
    private ImpactEquipementFacade referentielFacade;

    private ImpactEquipementCsvExportService csvExportService;

    @SneakyThrows
    @Override
    public ImpactEquipementDTO get(String refEquipement, String critere, String etapeacv) {

        ImpactEquipementIdDTO id = ImpactEquipementIdDTO.builder()
                .refEquipement(StringUtils.trim(refEquipement))
                .critere(critere)
                .etape(etapeacv)
                .build();
        try {
            return referentielFacade.get(id);
        } catch (ReferentielException e) {
            log.error("Erreur de recherche unitaire d'ImpactEquipement", e);
            throw new ReferentielException(e.getMessage());
        }
    }

    @Override
    public RapportImportDTO importCSV(MultipartFile fichier) throws IOException, ReferentielException {

        if (fichier == null || fichier.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Le fichier n'existe pas ou alors il est vide");
        }
        var rapportImport = new ImportImpactEquipementPortImpl().importCSV(fichier.getInputStream());
        referentielFacade.purgeAndAddAll(rapportImport.getObjects());

        return new RapportImportDTO(
                fichier.getOriginalFilename(),
                rapportImport.getErreurs(),
                rapportImport.getNbrLignesImportees()
        );

    }

    @Override
    public void exportCSV(HttpServletResponse servletResponse) throws IOException {
        exportCSV(servletResponse, csvExportService, "impactEquipement");
    }
}
