package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.domain.model.Etape;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface EtapeMapper {
    List<Etape> toDomain(List<EtapeDTO> etapesDTO);

    List<Etape> toDomains(List<EtapeEntity> etapesEntities);

    EtapeEntity toEntity(Etape referentiel);

    List<EtapeEntity> toEntities(Collection<Etape> referentiel);

    List<EtapeDTO> toDTO(List<Etape> etapes);
}
