package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import java.io.Serializable;

public interface AbstractReferentielEntity extends Serializable {
    // Actuellement l'interface n'a pas de comportement par défaut ni de champ partagé
}
