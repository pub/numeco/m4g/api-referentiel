package org.mte.numecoeval.referentiel.infrastructure.restapi.controller.correspondance;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.api.dto.ErrorResponseDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface ReferentielCorrespondanceRefEquipementRestApi {

    @Operation(
            summary = "Endpoint interne à NumEcoEval - Récupération d'une correspondance de refEquipement à partir d'un modèle d'équipement.",
            description = """
                    Endpoint interne utilisé dans l'import de données d'entrées dans NumEcoEval
                    pour déterminer la référence d'équipement à utiliser dans les référentiels'.
                    """,
            tags = "Interne NumEcoEval",
            operationId = "getCorrespondanceRefEquipement"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Correspondance trouvée",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CorrespondanceRefEquipementDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Correspondance non trouvée", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ErrorResponseDTO.class))})})
    @GetMapping(path = "/referentiel/correspondanceRefEquipement", produces = MediaType.APPLICATION_JSON_VALUE)
    CorrespondanceRefEquipementDTO get(@RequestParam(name = "modele") final String modele);

}
