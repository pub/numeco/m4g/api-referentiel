package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.domain.model.CorrespondanceRefEquipement;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CorrespondanceRefEquipementMapper {

    CorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementEntity entity);
    CorrespondanceRefEquipement toDomain(CorrespondanceRefEquipementDTO dto);

    CorrespondanceRefEquipementEntity toEntity(CorrespondanceRefEquipement domain);
    List<CorrespondanceRefEquipementEntity> toEntities(Collection<CorrespondanceRefEquipement> domains);

    CorrespondanceRefEquipementDTO toDto(CorrespondanceRefEquipement domain);


}
