package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.referentiel.api.dto.ImpactReseauDTO;
import org.mte.numecoeval.referentiel.domain.model.ImpactReseau;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactReseauId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactReseauIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")

public interface ImpactReseauMapper {

    ImpactReseauEntity toEntity(ImpactReseau impactReseau);

    @Mapping(source = "etape", target = "etapeACV")
    ImpactReseauDTO toDTO(ImpactReseau impactReseau);

    ImpactReseauIdEntity toEntityId(ImpactReseauId reseauId);

    ImpactReseau toDomain(ImpactReseauEntity reseauEntity);

    @Mapping(source = "etapeACV", target = "etape")
    ImpactReseau toDomain(ImpactReseauDTO impactReseauDTO);

    @Mapping(source = "etapeACV", target = "etape")
    ImpactReseauId toDomainId(ImpactReseauIdDTO idImpactReseauDTO);

    List<ImpactReseauEntity> toEntity(Collection<ImpactReseau> facteursImpacts);

    List<ImpactReseau> toDomains(List<ImpactReseauEntity> entities);

    List<ImpactReseau> toDomainsFromDTO(List<ImpactReseauDTO> dtos);
}
