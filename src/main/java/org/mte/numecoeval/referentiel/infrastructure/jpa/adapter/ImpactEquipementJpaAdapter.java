package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactEquipementIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactEquipementRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.ImpactEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@AllArgsConstructor
public class ImpactEquipementJpaAdapter implements ReferentielPersistencePort<ImpactEquipement, ImpactEquipementId> {

    private ImpactEquipementRepository repository;

    private ImpactEquipementMapper mapper;


    @Override
    public ImpactEquipement save(ImpactEquipement referentiel) throws ReferentielException {
        var entityToSave = mapper.toEntity(referentiel);
        if(entityToSave != null) {
            var entitySaved = repository.save(entityToSave);
            return mapper.toDomain(entitySaved);
        }
        return null;
    }

    @Override
    public void saveAll(Collection<ImpactEquipement> referentiel) throws ReferentielException {
        purge();
        repository.saveAll(mapper.toEntities(referentiel));
    }

    @Override
    public ImpactEquipement get(ImpactEquipementId id) throws ReferentielException {
        if(id != null) {
            ImpactEquipementIdEntity ieIdEntity = mapper.toEntityId(id);
            Optional<ImpactEquipementEntity> optionalEntity = repository.findById(ieIdEntity);
            return mapper.toDomain(
                    optionalEntity
                            .orElseThrow(() -> new ReferentielException("Impact Equipement non trouvé"))
            );
        }
        throw new ReferentielException("Impact Equipement non trouvé");
    }

    @Override
    public void purge() {
        repository.deleteAll();
    }

    @Override
    public List<ImpactEquipement> getAll() {
        return ListUtils.emptyIfNull(mapper.toDomains(repository.findAll()));
    }
}
