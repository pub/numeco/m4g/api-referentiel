package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.EtapeEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.EtapeIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "Etape" , itemResourceRel = "Etapes")
@Tag(name = "Etapes - CRUD/Spring Data REST")
public interface EtapeRepository extends JpaRepository<EtapeEntity, EtapeIdEntity> {

}
