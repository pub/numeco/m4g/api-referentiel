package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Critere;
import org.mte.numecoeval.referentiel.domain.model.id.CritereId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.CritereRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.CritereMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
@Slf4j
@AllArgsConstructor
public class CritereJpaAdapter implements ReferentielPersistencePort<Critere, CritereId> {

    CritereRepository critereRepository;

    // Pour les purges des données filles
    ImpactEquipementJpaAdapter impactEquipementJpaAdapter;
    ImpactReseauJpaAdapter impactReseauJpaAdapter;
    MixElectriqueJpaAdapter mixElectriqueJpaAdapter;

    ImpactMessagerieJpaAdapter impactMessagerieJpaAdapter;

    CritereMapper critereMapper;


    @Override
    public Critere save(Critere referentiel) throws ReferentielException {
        return null;
    }

    @Override
    public void saveAll(Collection<Critere> referentiel) throws ReferentielException {
        critereRepository.saveAll(critereMapper.toEntities(referentiel));
    }

    @Override
    public Critere get(CritereId id) throws ReferentielException {
        return null;
    }

    @Override
    public void purge() {
        // Purge des données parentes
        critereRepository.deleteAll();
    }

    @Override
    public List<Critere> getAll() {
        return critereMapper.toDomains(critereRepository.findAll());
    }
}
