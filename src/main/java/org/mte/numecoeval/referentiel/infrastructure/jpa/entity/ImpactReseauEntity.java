package org.mte.numecoeval.referentiel.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@IdClass(ImpactReseauIdEntity.class)
@Table(name = "REF_IMPACTRESEAU")
public class ImpactReseauEntity implements AbstractReferentielEntity {
    @Id
    @Column(name = "REFRESEAU", nullable = false)
    String refReseau;
    @Id
    @Column(name = "ETAPEACV", nullable = false)
    String etape;
    @Id
    @Column(name = "NOMCRITERE", nullable = false)
    String critere;
    String source;
    Double valeur;
    @Column(name = "CONSOELECMOYENNE")
    Double consoElecMoyenne;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ImpactReseauEntity that = (ImpactReseauEntity) o;

        return new EqualsBuilder().append(refReseau, that.refReseau).append(etape, that.etape).append(critere, that.critere).append(source, that.source).append(valeur, that.valeur).append(consoElecMoyenne, that.consoElecMoyenne).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(refReseau).append(etape).append(critere).append(source).append(valeur).append(consoElecMoyenne).toHashCode();
    }
}
