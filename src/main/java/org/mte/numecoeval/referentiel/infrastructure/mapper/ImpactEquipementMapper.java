package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.domain.model.ImpactEquipement;
import org.mte.numecoeval.referentiel.domain.model.id.ImpactEquipementId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactEquipementIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.ImpactEquipementIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ImpactEquipementMapper {
    ImpactEquipementId toDomainId(ImpactEquipementIdDTO id);

    ImpactEquipementEntity toEntity(ImpactEquipement referentiel);

    List<ImpactEquipementEntity> toEntities(Collection<ImpactEquipement> referentiel);

    ImpactEquipementIdEntity toEntityId(ImpactEquipementId id);

    ImpactEquipement toDomain(ImpactEquipementEntity entity);

    List<ImpactEquipement> toDomains(List<ImpactEquipementEntity> entities);

    ImpactEquipement toDomain(ImpactEquipementDTO dto);

    List<ImpactEquipement> toDomainsFromDTO(List<ImpactEquipementDTO> iesDTO);

    ImpactEquipementDTO toDTO(ImpactEquipement impactEquipement);
}
