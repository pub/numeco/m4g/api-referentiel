package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactReseauEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactReseauIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "ImpactReseau" , itemResourceRel = "ImpactReseaux")
@Tag(name = "ImpactReseau - CRUD/Spring Data REST")
public interface ImpactReseauRepository extends JpaRepository<ImpactReseauEntity, ImpactReseauIdEntity> {

}
