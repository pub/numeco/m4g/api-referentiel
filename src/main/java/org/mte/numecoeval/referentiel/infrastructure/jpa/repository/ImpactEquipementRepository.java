package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.ImpactEquipementIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "ImpactEquipement" , itemResourceRel = "ImpactEquipements")
@Tag(name = "ImpactEquipement - CRUD/Spring Data REST")
public interface ImpactEquipementRepository extends JpaRepository<ImpactEquipementEntity, ImpactEquipementIdEntity> {

}
