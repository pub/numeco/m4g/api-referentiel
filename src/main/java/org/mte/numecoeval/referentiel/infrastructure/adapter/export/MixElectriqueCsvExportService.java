package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportMixElectriquePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.MixElectriqueRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class MixElectriqueCsvExportService extends ReferentielCsvExportService<MixElectriqueEntity> {

    private final MixElectriqueRepository repository;

    public MixElectriqueCsvExportService(MixElectriqueRepository repository) {
        super(MixElectriqueEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportMixElectriquePortImpl.getHeaders();
    }

    @Override
    public List<MixElectriqueEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(MixElectriqueEntity object) {
        return String.join("-", object.getPays(), object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, MixElectriqueEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(
                objectToWrite.getPays(),
                objectToWrite.getRaccourcisAnglais(),
                objectToWrite.getCritere(),
                formatDouble(objectToWrite.getValeur()),
                objectToWrite.getSource()
                );
    }

}
