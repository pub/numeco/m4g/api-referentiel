package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.HypotheseEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.HypotheseIdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;


@RepositoryRestResource(path = "Hypothèses" , itemResourceRel = "Hypothèses")
@Tag(name = "Hypothèse - CRUD/Spring Data REST")
public interface HypotheseRepository extends JpaRepository<HypotheseEntity, HypotheseIdEntity> {

    Optional<HypotheseEntity> findById(HypotheseIdEntity id);
}
