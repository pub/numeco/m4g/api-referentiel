package org.mte.numecoeval.referentiel.infrastructure.jpa.repository;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.CorrespondanceRefEquipementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "CorrespondanceRefEquipement" , itemResourceRel = "CorrespondancesRefEquipement")
@Tag(name = "CorrespondanceRefEquipement")
public interface CorrespondanceRefEquipementRepository extends JpaRepository<CorrespondanceRefEquipementEntity,String> {
}
