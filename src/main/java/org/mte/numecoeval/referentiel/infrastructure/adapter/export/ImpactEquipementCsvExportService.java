package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportImpactEquipementPortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.ImpactEquipementEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.ImpactEquipementRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ImpactEquipementCsvExportService extends ReferentielCsvExportService<ImpactEquipementEntity> {

    private final ImpactEquipementRepository repository;

    public ImpactEquipementCsvExportService(ImpactEquipementRepository repository) {
        super(ImpactEquipementEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportImpactEquipementPortImpl.getHeaders();
    }

    @Override
    public List<ImpactEquipementEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(ImpactEquipementEntity object) {
        return String.join("-", object.getRefEquipement(),object.getEtape(),object.getCritere());
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, ImpactEquipementEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getRefEquipement(),
                objectToWrite.getEtape(),objectToWrite.getCritere(),
                formatDouble(objectToWrite.getConsoElecMoyenne()),formatDouble(objectToWrite.getValeur()),
                objectToWrite.getSource(), objectToWrite.getType()
                );
    }

}
