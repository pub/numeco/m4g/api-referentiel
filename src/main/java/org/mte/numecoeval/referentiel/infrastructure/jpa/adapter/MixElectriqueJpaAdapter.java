package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.MixElectrique;
import org.mte.numecoeval.referentiel.domain.model.id.MixElectriqueId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.MixElectriqueEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.MixElectriqueIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.MixElectriqueRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.MixElectriqueMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@AllArgsConstructor
public class MixElectriqueJpaAdapter implements ReferentielPersistencePort<MixElectrique, MixElectriqueId> {
    MixElectriqueRepository mixElectriqueRepository;
    MixElectriqueMapper mixElectriqueMapper;


    @Override
    public MixElectrique save(MixElectrique referentiel) throws ReferentielException {
        var entityToSave = mixElectriqueMapper.toEntity(referentiel);
        if(entityToSave != null) {
            var entitySaved = mixElectriqueRepository.save(entityToSave);
            return mixElectriqueMapper.toDomain(entitySaved);
        }
        return null;
    }

    @Override
    public void saveAll(Collection<MixElectrique> mixElecs) throws ReferentielException {
        List<MixElectriqueEntity> mixElecsEntities = mixElectriqueMapper.toEntities(mixElecs);
        mixElectriqueRepository.saveAll(mixElecsEntities);
    }


    @Override
    public MixElectrique get(MixElectriqueId id) throws ReferentielException {
        if(id != null) {
            MixElectriqueIdEntity meIdEntity = mixElectriqueMapper.toEntityId(id);
            Optional<MixElectriqueEntity> reseauEntityOptional = mixElectriqueRepository.findById(meIdEntity);
            return mixElectriqueMapper.toDomain(
                    reseauEntityOptional
                            .orElseThrow(() -> new ReferentielException("Mix Electrique non trouvé"))
            );
        }
        throw new ReferentielException("Mix Electrique non trouvé");
    }

    @Override
    public void purge() {
        mixElectriqueRepository.deleteAll();
    }

    @Override
    public List<MixElectrique> getAll() {
        return ListUtils.emptyIfNull(mixElectriqueMapper.toDomains(mixElectriqueRepository.findAll()));
    }

}
