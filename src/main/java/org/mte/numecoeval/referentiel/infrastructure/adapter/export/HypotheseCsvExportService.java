package org.mte.numecoeval.referentiel.infrastructure.adapter.export;

import org.apache.commons.csv.CSVPrinter;
import org.mte.numecoeval.referentiel.domain.ports.input.impl.ImportHypothesePortImpl;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielCsvExportService;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.HypotheseEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.HypotheseRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class HypotheseCsvExportService extends ReferentielCsvExportService<HypotheseEntity> {

    private final HypotheseRepository repository;

    public HypotheseCsvExportService(HypotheseRepository repository) {
        super(HypotheseEntity.class);
        this.repository = repository;
    }

    @Override
    public String[] getHeaders() {
        return ImportHypothesePortImpl.getHeaders();
    }

    @Override
    public List<HypotheseEntity> getObjectsToWrite() {
        return repository.findAll();
    }

    @Override
    protected String getObjectId(HypotheseEntity object) {
        return object.getCode();
    }

    @Override
    public void printRecord(CSVPrinter csvPrinter, HypotheseEntity objectToWrite) throws IOException {
        csvPrinter.printRecord(objectToWrite.getCode(),objectToWrite.getValeur(),objectToWrite.getSource());
    }

}
