package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Hypothese;
import org.mte.numecoeval.referentiel.domain.model.id.HypotheseId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.HypotheseMapper;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.HypotheseIdDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class HypotheseFacade {

    private ReferentielPersistencePort<Hypothese, HypotheseId> persistencePort;


    private HypotheseMapper mapper;

    /**
     * Ajout des hypotheses depuis un fichier csv
     *
     * @param hypotheses
     */
    public void purgeAndAddAll(List<HypotheseDTO> hypotheses) throws ReferentielException {
        persistencePort.purge();
        persistencePort.saveAll(mapper.toDomains(hypotheses));
    }

    /**
     * Recuperation de la liste des hypotheses
     *
     * @return
     */
    public List<HypotheseDTO> getAll() {
        return mapper.toDtos(persistencePort.getAll());
    }


    public HypotheseDTO get(HypotheseIdDTO hypotheseIdDTO) throws ReferentielException {
        Hypothese hypothese = persistencePort.get(mapper.toDomain(hypotheseIdDTO));
        return mapper.toDTO(hypothese);
    }
}
