package org.mte.numecoeval.referentiel.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.referentiel.api.dto.HypotheseDTO;
import org.mte.numecoeval.referentiel.domain.model.Hypothese;
import org.mte.numecoeval.referentiel.domain.model.id.HypotheseId;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.HypotheseEntity;
import org.mte.numecoeval.referentiel.infrastructure.jpa.entity.id.HypotheseIdEntity;
import org.mte.numecoeval.referentiel.infrastructure.restapi.dto.id.HypotheseIdDTO;

import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface HypotheseMapper {

    HypotheseEntity toEntity(Hypothese referentiel);

    List<HypotheseEntity> toEntities(Collection<Hypothese> referentiel);

    List<Hypothese> fromEntities(List<HypotheseEntity> all);


    Collection<Hypothese> toDomains(List<HypotheseDTO> hypotheses);

    List<HypotheseDTO> toDtos(List<Hypothese> all);


    HypotheseId toDomain(HypotheseIdDTO hypotheseIdDTO);

    HypotheseDTO toDTO(Hypothese hypothese);


    HypotheseIdEntity toEntityId(HypotheseId id);

    Hypothese toDomain(HypotheseEntity hypothese);
}
