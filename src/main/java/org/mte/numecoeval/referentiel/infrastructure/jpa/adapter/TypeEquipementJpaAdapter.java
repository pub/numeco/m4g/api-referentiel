package org.mte.numecoeval.referentiel.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.TypeEquipement;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.jpa.repository.TypeEquipementRepository;
import org.mte.numecoeval.referentiel.infrastructure.mapper.TypeEquipementMapper;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class TypeEquipementJpaAdapter implements ReferentielPersistencePort<TypeEquipement,String> {

    TypeEquipementMapper typeEquipementMapper;
    TypeEquipementRepository typeEquipementRepository;

    @Override
    public TypeEquipement save(TypeEquipement referentiel) throws ReferentielException {
        typeEquipementRepository.save(typeEquipementMapper.toEntity(referentiel));
        return null;
    }

    @Override
    public void saveAll(Collection<TypeEquipement> referentiels) throws ReferentielException {
        typeEquipementRepository.saveAll(ListUtils.emptyIfNull(referentiels
                .stream()
                .map(typeEquipement -> typeEquipementMapper.toEntity(typeEquipement))
                .toList()));
    }

    @Override
    public TypeEquipement get(String id) throws ReferentielException {
        var entityOpt=  typeEquipementRepository.findById(id);
        if(entityOpt.isPresent()){
            return typeEquipementMapper.toDomaine(entityOpt.get());
        }
        return null;
    }

    @Override
    public void purge() {
        typeEquipementRepository.deleteAll();
    }

    @Override
    public List<TypeEquipement> getAll() {

        return ListUtils.emptyIfNull(typeEquipementRepository.findAll())
                .stream()
                .map(typeEquipementEntity -> typeEquipementMapper.toDomaine(typeEquipementEntity))
                .toList();
    }
}
