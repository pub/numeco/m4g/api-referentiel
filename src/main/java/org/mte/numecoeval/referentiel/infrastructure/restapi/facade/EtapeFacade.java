package org.mte.numecoeval.referentiel.infrastructure.restapi.facade;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.referentiel.api.dto.EtapeDTO;
import org.mte.numecoeval.referentiel.domain.exception.ReferentielException;
import org.mte.numecoeval.referentiel.domain.model.Etape;
import org.mte.numecoeval.referentiel.domain.model.id.EtapeId;
import org.mte.numecoeval.referentiel.domain.ports.output.ReferentielPersistencePort;
import org.mte.numecoeval.referentiel.infrastructure.mapper.EtapeMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class EtapeFacade {

    private ReferentielPersistencePort<Etape, EtapeId> persistencePort;

    private EtapeMapper mapper;


    /**
     * Ajout en masse des etapes
     *
     * @param etapesDTO
     */
    public void purgeAndAddAll(List<EtapeDTO> etapesDTO) throws ReferentielException {
        persistencePort.purge();
        persistencePort.saveAll(mapper.toDomain(etapesDTO));
    }

    /**
     * @return
     */
    public List<EtapeDTO> getAll() {
        return mapper.toDTO(persistencePort.getAll());
    }
}
